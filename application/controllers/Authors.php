<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authors extends CI_Controller {

	public function __construct(){
		parent::__construct ();
		$this->load->model('common_model', 'db_common' );
		$this->load->library('email'); 
	}
	public function index(){		
		$data['allData'] = $this->db->order_by("authID", "desc")->get('blog_authors')->result();
		$this->load->template_front('front/authors',$data);
	}
}