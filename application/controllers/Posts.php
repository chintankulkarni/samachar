<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Posts extends CI_Controller {

	public function __construct(){
		parent::__construct ();
		$this->load->model('common_model', 'db_common' );		
		$this->load->library('pagination');
	}
	public function index(){	
		$config['base_url'] = base_url().'/Posts';
		$config['total_rows'] = $this->db->count_all("blog_posts_seo");
		$config['per_page'] = 20;
		$config["uri_segment"] = 3;
		$config['use_page_numbers'] = TRUE;
		$config['page_query_string'] = TRUE;

		 $config['full_tag_open'] = ' <nav class="pagination">';
		  $config['full_tag_close'] = '</nav>';

		  $config['first_link'] = '« First';
		  $config['first_tag_open'] = '<span class="page-numbers">';
		  $config['first_tag_close'] = '</span>';

		  $config['last_link'] = 'Last »';
		  $config['last_tag_open'] = '<span class="page-numbers">';
		  $config['last_tag_close'] = '</span>';

		  $config['next_link'] = 'Next →';
		  $config['next_tag_open'] = '<span class="page-numbers">';
		  $config['next_tag_close'] = '</span>';

		  $config['prev_link'] = '← Previous';
		  $config['prev_tag_open'] = '<span class="page-numbers">';
		  $config['prev_tag_close'] = '</span>';

		  $config['cur_tag_open'] = '<span class="page-numbers">';
		  $config['cur_tag_close'] = '</span>';

		  $config['num_tag_open'] = '<span class="page-numbers">';
		  $config['num_tag_close'] = '</span>';

		$page = isset($_GET['per_page']) ? (($_GET['per_page']) - 1) : 0;
		echo $page;

		$this->pagination->initialize($config);
		$data['allData'] = $this->db->order_by("postID", "desc")->limit($config['per_page'], $page)->get('blog_posts_seo')->result();
		// var_dump(count($data['allData']));
		// echo $this->pagination->create_links();
		//exit();
		$this->load->template_front('front/posts',$data);
	}
	public function news(){
		$id = $this->uri->segment(3, 0);
		$data['next'] = $this->db->where('postID >',$id)->order_by("postID", "asc")->limit(1,0)->get('blog_posts_seo')->result();

		$data['current'] = $this->db->get_where('blog_posts_seo', array('postID' => $id))->result();

		$data['previous'] = $this->db->where('postID <',$id)->order_by("postID", "desc")->limit(1,0)->get('blog_posts_seo')->result();
		
		$this->load->template_front('front/news',$data);
	}
}