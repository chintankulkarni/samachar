<?php
/**
 * Created by PhpStorm.
 * User: Niyati
 * Date: 21-Nov-14
 * Time: 3:10 PM
 */
class PRONTOJI_Output extends CI_Output{
    public function nocache(){
        $this->set_header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
        $this->set_header('Cache-Control: no-cache, no-store, must-revalidate, max-age=0');
        $this->set_header('Cache-Control: post-check=0, pre-check=0', FALSE);
        $this->set_header('Pragma: no-cache');
    }
}