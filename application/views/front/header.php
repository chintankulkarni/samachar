<!DOCTYPE html>
<!--[if IE 8 ]><html class="no-js oldie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="no-js oldie ie9" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>

   <!--- basic page needs
   ================================================== -->
   <meta charset="utf-8">
    <title>Samacharlive::authors</title>
    <meta name="description" content="">  
    <meta name="author" content="">

   <!-- mobile specific metas
   ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
   ================================================== -->
   
   <link rel="stylesheet" href="<?php echo base_url('assets/front'); ?>/css/base.css">
   <link rel="stylesheet" href="<?php echo base_url('assets/front'); ?>/css/vendor.css">  
   <link rel="stylesheet" href="<?php echo base_url('assets/front'); ?>/css/main.css">
        
 <script src="<?php echo base_url('assets/front'); ?>/js/jquery-2.1.3.min.js"></script>
   <!-- script
   ================================================== -->
    <script src="<?php echo base_url('assets/front'); ?>/js/modernizr.js"></script>
    <script src="<?php echo base_url('assets/front'); ?>/js/pace.min.js"></script>

   <!-- favicons
    ================================================== -->
    <link rel="shortcut icon" href="<?php echo base_url('assets/front'); ?>/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url('assets/front'); ?>/favicon.ico" type="image/x-icon">

</head>

<body id="top">

    <!-- header 
   ================================================== -->
   <header class="short-header">   

    <div class="gradient-block"></div>  

    <div class="row header-content">

        <div class="logo">
             <a href="<?php echo base_url('assets/front'); ?>/index.html">Author</a>
          </div>

        <nav id="main-nav-wrap">
                <ul class="main-navigation sf-menu">
                    <li class="current"><a href="#" title="">Home</a></li>                        
                    <li><a href="#" title="">India</a></li>
                    <li><a href="#" title="">World Business</a></li>    
                    <li><a href="#" title="">Entertainment</a></li>                                 
               <li><a href="#" title="">Life Style</a></li>                          
               <li><a href="#" title="">Travel</a></li>
               <li><a href="#" title="">हिनदी</a></li>
                </ul>
            </nav> <!-- end main-nav-wrap -->   
         <div class="triggers">            
            <a class="menu-toggle" href="#"><span>Menu</span></a>
         </div>     
    </div>          
    
   </header> <!-- end header -->