   <!-- masonry
   ================================================== -->
   <section id="bricks">

      <div class="row latest-wrapper">
            <div class="col-twelve p-none">
                <div class="latest-title">
                    Latest News
                </div>
                <div class="latest-news-marquee">
                  <marquee>
                    <a href="">Odisha to present Rs 9,829 cr supplementary budget</a>
                    <span class="seperator">|</span>
                    <a href="">Samsung to soon combine IoT Cloud with its products</a>
                    <span class="seperator">|</span>
                    <a href="">Telangana to lau</a>
                </marquee> 
                </div>
            </div>
        </div>

    <div class="row masonry">

      <!-- brick-wrapper -->
         <div class="bricks-wrapper">

          <div class="grid-sizer"></div>

          <div class="brick entry featured-grid p-r-none animate-this">
            <div class="entry-content">
              <div id="featured-post-slider" class="flexslider">
              <ul class="slides">

                <li>
                  <div class="featured-post-slide">

                    <div class="post-background" style="background-image:url('<?php echo base_url('assets/front'); ?>/images/thumbs/featured/news-banner.jpg');"></div>

                    <div class="overlay"></div>           

                    <div class="post-content">
                      <h1 class="slide-title"><a href="#" title="">Nation should ensure digital space does not become playground of dark forces: Modi</a></h1> 
                                 <ul class="entry-meta">
                                    <li><span class="green">Author:</span> Indo-Asian News Service</li> 
                                    <li><span class="yellow">Updated:</span> 23rd Nov 2017 01:30:10 PM</li>           
                                 </ul> 
                    </div>                      
                
                  </div>
                </li> <!-- /slide -->

                <li>
                  <div class="featured-post-slide">

                    <div class="post-background" style="background-image:url('<?php echo base_url('assets/front'); ?>/images/thumbs/featured/featured-3.jpg');"></div>

                    <div class="overlay"></div>           

                    <div class="post-content">
                                 <h1 class="slide-title"><a href="#" title="">Nation should ensure digital space does not become playground of dark forces: Modi</a></h1> 
                                 <ul class="entry-meta">
                                    <li><span class="green">Author:</span> Indo-Asian News Service</li> 
                                    <li><span class="yellow">Updated:</span> 23rd Nov 2017 01:30:10 PM</li>           
                                 </ul> 
                              </div>                                
                
                  </div>
                </li> <!-- /slide -->

                <li>
                  <div class="featured-post-slide">

                    <div class="post-background" style="background-image:url('<?php echo base_url('assets/front'); ?>/images/thumbs/featured/featured-1.jpg');"></div>

                    <div class="overlay"></div>           

                    <div class="post-content">
                                 <h1 class="slide-title"><a href="#" title="">Nation should ensure digital space does not become playground of dark forces: Modi</a></h1> 
                                 <ul class="entry-meta">
                                    <li><span class="green">Author:</span> Indo-Asian News Service</li> 
                                    <li><span class="yellow">Updated:</span> 23rd Nov 2017 01:30:10 PM</li>           
                                 </ul> 
                              </div>                                

                  </div>
                </li> <!-- end slide -->

              </ul> <!-- end slides -->
            </div> <!-- end featured-post-slider -->              
            </div> <!-- end entry content -->             
          </div>
            <?php if(count($allData) > 0){
               foreach ($allData as $value) {                  
             ?>
            <article class="brick entry animate-this">
                  <div class="entry-text">
                  <div class="entry-header">

                     <h1 class="entry-title"><a href="<?php echo base_url('Posts/news').'/'.$value->postID; ?>"><?php echo $value->postTitle;?><i class="fa fa-angle-right pull-right" aria-hidden="true"></i></a></h1>
                     
                  </div>
                  <div class="entry-excerpt">
                     <ul>
                        <li><a href="<?php echo base_url('Posts/news').'/'.$value->postID; ?>"><?php echo substr(strip_tags($value->postCont),0,300)."..." ?></a></li>
                     </ul>
                  </div>
                  </div>
               
            </article> <!-- end article -->

            <?php }
             } ?>
          

         </div> <!-- end brick-wrapper --> 

    </div> <!-- end row -->

    <div class="row">

        <?php  echo $this->pagination->create_links(); ?>

    </div>

   </section> <!-- end bricks -->