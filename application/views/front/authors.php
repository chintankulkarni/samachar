  <!-- masonry
   ================================================== -->
   <section id="bricks">
      <div class="row latest-wrapper">
            <div class="col-twelve p-none">
                <div class="latest-title">
                    Latest News
                </div>
                <div class="latest-news-marquee">
                  <marquee>
                    <a href="">Odisha to present Rs 9,829 cr supplementary budget</a>
                    <span class="seperator">|</span>
                    <a href="">Samsung to soon combine IoT Cloud with its products</a>
                    <span class="seperator">|</span>
                    <a href="">Telangana to lau</a>
                </marquee> 
                </div>
            </div>
        </div>

   	<div class="row masonry">
         <div class="col-nine">
   		<!-- brick-wrapper -->
          <div class="bricks-wrapper">

         	<div class="grid-sizer"></div>
            <?php if(count($allData) > 0){
               foreach ($allData as $value) {                  
             ?>
            <!-- format audio here -->
            <article class="brick three entry format-audio animate-this author-article">

               <div class="entry-thumb">
                  <a class="thumb-link">
                     <img src="<?php echo $value->authorImg; ?>" alt="concert">
                  </a>
               </div>

               <div class="entry-text">
                  <div class="entry-header">
                     <h1 class="entry-title p-none m-none"><a href="#"><?php echo $value->authorName; ?></a></h1>
                  </div>
                  <div class="entry-excerpt">                     
                     <p>
                        <span>About Author : </span><?php echo $value->authorDesc; ?>
                     </p>
                     <div class="social-links">
                        <a href="<?php echo $value->authorFB; ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="<?php echo $value->authorTW; ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                       <!--  <a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                        <a href=""><i class="fa fa-youtube-play" aria-hidden="true"></i></a> -->
                        <a href="<?php echo $value->authorLN; ?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                    </div>
                  </div>
               </div>
               
            </article> <!-- /article -->
            <?php }
             } ?>
            
         </div>
         	
            
         </div> <!-- end brick-wrapper --> 
         <div class="col-three">
               <article class="brick entry format-standard animate-this p-l-none first-article">
                  <div class="entry-text">
                     <div class="entry-header">

                        <h1 class="entry-title"><a href="#">LATEST NEWS <i class="fa fa-angle-right pull-right" aria-hidden="true"></i></a></h1>
                        
                     </div>
                     <div class="entry-excerpt">
                        <ul>
                           <li><a href="">Odisha to present Rs 9,829 cr supplementary budget</a></li>
                           <li><a href="">Samsung to soon combine IoT Cloud with its products</a></li>
                           <li><a href="">Telangana to launch electric vehicle policy next month</a></li>
                           <li><a href="">IndiGo to add 8 extra flights from January 1</a></li>
                           <li><a href="">Congress seeks President’s intervention to convene</a></li>
                        </ul>
                     </div>
                  </div>

               </article> <!-- end article -->
               <article class="brick entry format-standard animate-this p-l-none m-t-40">
                  <div class="entry-text">
                     <div class="entry-header">

                        <h1 class="entry-title"><a href="#">LATEST NEWS <i class="fa fa-angle-right pull-right" aria-hidden="true"></i></a></h1>
                        
                     </div>
                     <div class="entry-excerpt">
                        <ul>
                           <li><a href="">Odisha to present Rs 9,829 cr supplementary budget</a></li>
                           <li><a href="">Samsung to soon combine IoT Cloud with its products</a></li>
                           <li><a href="">Telangana to launch electric vehicle policy next month</a></li>
                           <li><a href="">IndiGo to add 8 extra flights from January 1</a></li>
                           <li><a href="">Congress seeks President’s intervention to convene</a></li>
                        </ul>
                     </div>
                  </div>

               </article> <!-- end article -->
            </div>   

   	</div> <!-- end row -->

   	<!-- <div class="row">
   		
   		<nav class="pagination">
		      <span class="page-numbers prev inactive">Prev</span>
		   	<span class="page-numbers current">1</span>
		   	<a href="#" class="page-numbers">2</a>
		      <a href="#" class="page-numbers">3</a>
		      <a href="#" class="page-numbers">4</a>
		      <a href="#" class="page-numbers">5</a>
		      <a href="#" class="page-numbers">6</a>
		      <a href="#" class="page-numbers">7</a>
		      <a href="#" class="page-numbers">8</a>
		      <a href="#" class="page-numbers">9</a>
		   	<a href="#" class="page-numbers next">Next</a>
	      </nav>

   	</div> -->

   </section> <!-- end bricks -->

   