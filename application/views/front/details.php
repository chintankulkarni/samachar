<link href="<?php echo base_url(); ?>assets/front/plugins/seiyria-bootstrap-slider/dist/css/bootstrap-slider.min.css" rel="stylesheet">
   
<![endif]-->
    <style type="text/css">
    .gm-style .gm-style-mtc label,
    .gm-style .gm-style-mtc div {
        font-weight: 400
    }
    </style>
    <style type="text/css">
    .gm-style .gm-style-cc span,
    .gm-style .gm-style-cc a,
    .gm-style .gm-style-mtc div {
        font-size: 10px
    }

    .navbar {
        margin-bottom: 0px !important;
    }
    </style>
    <style type="text/css">
    @media print {
        .gm-style .gmnoprint,
        .gmnoprint {
            display: none
        }
    }

    @media screen {
        .gm-style .gmnoscreen,
        .gmnoscreen {
            display: none
        }
    }
    </style>
    <style type="text/css">
    .gm-style-pbc {
        transition: opacity ease-in-out;
        background-color: rgba(0, 0, 0, 0.45);
        text-align: center
    }

    .gm-style-pbt {
        font-size: 22px;
        color: white;
        font-family: Roboto, Arial, sans-serif;
        position: relative;
        margin: 0;
        top: 50%;
        -webkit-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        transform: translateY(-50%)
    }
    </style>
    <style type="text/css">
    @charset "UTF-8";
    [ng\:cloak],
    [ng-cloak],
    [data-ng-cloak],
    [x-ng-cloak],
    .ng-cloak,
    .x-ng-cloak,
    .ng-hide:not(.ng-hide-animate) {
        display: none !important;
    }

    ng\:form {
        display: block;
    }
    </style>
    
    <link href="<?php echo base_url(); ?>assets/front/detal-vendors/application-95a354f066dfd8c538cb4e414076c995.css" media="all" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/front/detal-vendors/place_detail-2c49e0e20f4a210e16919a2f7460daf1.css" media="all" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/front/detal-vendors/house-detail-new-b1174b8d98fd18e6438b8cd1e825ee3f.css" media="all" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/front/detal-vendors/daterangepicker-fe02a3cd5be7498c2d0f19d9fc76c8fd.css" media="screen" rel="stylesheet" type="text/css">

    <link href="<?php echo base_url(); ?>assets/front/plugins/seiyria-bootstrap-slider/dist/css/bootstrap-slider.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/front/css/listingpage.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/front/css/style.css" rel="stylesheet">
    <style>
    .current-tab{
        z-index: 1!important;
    }
</style>
    <div class="home-detail-widget">
        <!-- banner -->
        <div class="house-banner-container">
            <div class="house-banner-new banner-slider-popup" style="background-image: url(<?php echo base_url(); ?>assets/front/images/banner-detail.jpg)"></div>
            <!-- other banner details -->
            <div class="banner-detail-content">
                <div class="content-widget">
                    
                    <!-- price -->
                    <div class="banner-price-widget">
                        <div class="col-1-2">
                            <div class="col-1-2">
                                <span class="border-div">
                <span>For</span>
                                <strong>
                   <?php echo ($data[0]->for_rent == 1 ) ? 'Rent': 'Sell';?>
                </strong>
                                </span>
                            </div>
                            <div class="col-1-2">
                                <span>Amount</span>
                                <strong> ₹ <?php echo $data[0]->amount;?> </strong>
                            </div>
                            <div class="clr"></div>
                        </div>
                        <div class="col-1-2 text-right pad-r-zero">
                            <span class="house-offer-info scroll-nav">
            </span>
                        </div>
                        <div class="clr"></div>
                    </div>
                    <!-- price -->
                </div>
            </div>
            <!-- other banner details -->
        </div>
        <!-- banner -->
        <!-- main details -->
        <div class="house-detail-content">
            <div class="house-left-column-lg">
                <?php /*
                <!-- tab -->
                <div class="tab-navigation">
                    <div class="tab-nav-content">
                        <nav class="scroll-nav">
                            <ul>
                                <li>
                                    <a class="active" href="" onclick="utils.gaEventTracker(&#39;House Details Page New&#39;, &#39;Tabs Navigation&#39;, &#39;Overview Tab&#39;);">Overview</a>
                                </li>
                                <li>
                                    <a href="" onclick="utils.gaEventTracker(&#39;House Details Page New&#39;, &#39;Tabs Navigation&#39;, &#39;Pricing Tab&#39;);" class="">
                  Pricing
                </a>
                                </li>
                                <li>
                                    <a href="" onclick="utils.gaEventTracker(&#39;House Details Page New&#39;, &#39;Tabs Navigation&#39;, &#39;Amenities Tab&#39;);" class="">Amenities</a>
                                </li>
                                <li>
                                    <a href="" onclick="utils.gaEventTracker(&#39;House Details Page New&#39;, &#39;Tabs Navigation&#39;, &#39;House Details Tab&#39;);" class="">House Details</a>
                                </li>
                                <li>
                                    <a href="" onclick="utils.gaEventTracker(&#39;House Details Page New&#39;, &#39;Tabs Navigation&#39;, &#39;Neighbourhood Tab&#39;);" class="">Neighborhood</a>
                                </li>
                                <li id="similar-houses-tab" class="margin-r-zero">
                                    <a href="" onclick="utils.gaEventTracker(&#39;House Details Page New&#39;, &#39;Tabs Navigation&#39;, &#39;Similar Houses Tab&#39;);" class="">Similar Houses</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <div class="dummy-color animated fadeIn" style="display: none;"></div>
                </div>
                <!-- tab -->
                */ ?>
                <section id="overview" class="nav-section active">
                    <div class="overview">
                        <div class="overview-details">
                            <div class="col-9-12">
                                <h1>
                <span>
                  <?php echo $data[0]->property_name;?> <span style="font-size:20px;">₹</span><?php echo $data[0]->amount;?>, <?php echo $this->db->get_where('cities',['id'=> $data[0]->city_id])->result()[0]->city_name; ?>
                </span>
              </h1>
              <?php /*          <div class="other-info">
                                    <span>
                  <strong>Only for Boys</strong>
                </span>
                                    <span>
                    <strong>2 beds
                        available
                    </strong>
                </span>
                                </div>
                                 */ ?>
                            </div>
                            <?php /*
                            <div class="col-3-12">
                                <!-- owner information -->
                                <div class="owner-profile-widget flRt">
                                    <img src="<?php echo base_url(); ?>assets/front/detal-vendors/thumb_medium_662d00dd82.jpg" alt="Nathu Nest Delhi 3 BHK Flat Image for House N7860">
                                    <div class="details">
                                        <strong>
                      Manu
                    </strong>
                                        <span>House Owner</span>
                                    </div>
                                </div>
                                <!-- owner information -->
                            </div> 
                            */ ?>
                            <div class="clr"></div>
                        </div>
                    </div>
                    <!-- description -->
                    <div class="house-description">
                        <h2>About this property</h2> <?php echo $data[0]->description;?>
                    </div>
                    <!-- description -->
                </section>
                <!-- bed selection -->
                <section id="price" class="nav-section">
                    <div class="box-new bed-sele-widget">
                        <div class="content">
                            <h2>Price Details</h2>
                            <div class="bed-selection-tab">
                                <?php /*
                                <!-- tab heading -->
                                <div class="bed-selection-tab-list">
                                    <ul>
                                        <li class="active" data-tab="bed-tab">Single Bed</li>
                                        <li data-tab="bed-room">Private Room</li>
                                    </ul>
                                </div> 
                                */ ?>
                                <div style="display: block" class="selection-content" id="bed-tab">
                                    <div class="bed-selection-price-details">
                                        <ul>
                                            <li class="col-1-2">
                                                
                                                <strong><span><?php echo ($data[0]->for_rent == 1 ) ? 'Rent': 'Sell';?> ₹ <?php echo $data[0]->amount;?></span></strong>
                                            </li>
                                          <?php /*   <li class="col-1-2">
                                                <span>2 Months  Security Deposit</span>
                                                <strong><i class="icon-rupee"></i> 12000</strong>
                                            </li>  */ ?>
                                        </ul>
                                    </div>
                                 <?php /*   <!-- bed selection -->
                                    <div class="bed-selection-widget">
                                        <ul>
                                            <li class="animated fadeIn ac-rental-widget bed-selection">
                                                <i>Room 1
        </i>
                                                <div class="colume-bed">
                                                    <div class="show-tooltip sold" title="" data-original-title="Sold Out">
                                                        <span class="icons-bed-icon"></span>
                                                        <span class="price">Sold Out</span>
                                                    </div>
                                                </div>
                                                <div class="colume-bed">
                                                    <div class="show-tooltip" title="" data-original-title="Available from 21/02/2018">
                                                        <span class="icons-bed-icon"></span>
                                                        <span class="price">₹ 6500
                </span>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="animated fadeIn ac-rental-widget bed-selection">
                                                <i>Room 2
        </i>
                                                <div class="colume-bed">
                                                    <div class="show-tooltip" title="" data-original-title="Available from 21/02/2018">
                                                        <span class="icons-bed-icon"></span>
                                                        <span class="price">₹ 6000
                </span>
                                                    </div>
                                                </div>
                                                <div class="colume-bed">
                                                    <div class="show-tooltip sold" title="" data-original-title="Sold Out">
                                                        <span class="icons-bed-icon"></span>
                                                        <span class="price">Sold Out</span>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="animated fadeIn ac-rental-widget bed-selection">
                                                <i>Room 3
        </i>
                                                <div class="colume-bed">
                                                    <div class="show-tooltip sold" title="" data-original-title="Sold Out">
                                                        <span class="icons-bed-icon"></span>
                                                        <span class="price">Sold Out</span>
                                                    </div>
                                                </div>
                                                <div class="colume-bed">
                                                    <div class="show-tooltip sold" title="" data-original-title="Sold Out">
                                                        <span class="icons-bed-icon"></span>
                                                        <span class="price">Sold Out</span>
                                                    </div>
                                                </div>
                                            </li>
                                            <div class="toast-message" style="display:none"></div>
                                            <div class="clr"></div>
                                        </ul>
                                    </div> */ ?>
                                </div>
                                <div class="selection-content" id="bed-room">
                                    <div class="bed-selection-price-details">
                                        <ul>
                                            <li class="col-1-2">
                                                <span class="border-right">
        <span>Rent</span>
                                                <strong><i class="icon-rupee"></i> 12000</strong>
                                                </span>
                                            </li>
                                            <li class="col-1-2">
                                                <span>2 Months  Security Deposit</span>
                                                <strong><i class="icon-rupee"></i> 24000</strong>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- bed selection -->
                                    <div class="bed-selection-widget">
                                        <ul>
                                            <li class="animated fadeIn ac-rental-widget bed-selection">
                                                <i>Room 1
       </i>
                                                <div class="colume-bed colume-room">
                                                    <div class="show-tooltip sold" title="" data-original-title="Not Available">
                                                        <span class="bed-icon icons-icon-room"></span>
                                                        <span class="price">N A</span>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="animated fadeIn ac-rental-widget bed-selection">
                                                <i>Room 2
       </i>
                                                <div class="colume-bed colume-room">
                                                    <div class="show-tooltip sold" title="" data-original-title="Not Available">
                                                        <span class="bed-icon icons-icon-room"></span>
                                                        <span class="price">N A</span>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="animated fadeIn ac-rental-widget bed-selection">
                                                <i>Room 3
       </i>
                                                <div class="colume-bed colume-room">
                                                    <div class="show-tooltip sold" title="" data-original-title="Not Available">
                                                        <span class="bed-icon icons-icon-room"></span>
                                                        <span class="price">N A</span>
                                                    </div>
                                                </div>
                                            </li>
                                            <div class="toast-message" style="display:none"></div>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- offer section -->
                        <!-- offer section -->
                        <!-- cost of living start -->
                      <?php /*  <div class="monthly-exp-widget">
                            <div class="content">
                                <h3><b>Monthly Expenses</b></h3>
                                <div class="col-2-12">
                                    <img alt="Wallet-icon" src="<?php echo base_url(); ?>assets/front/detal-vendors/wallet-icon-549b9640822c28e205230fc1b083724d.svg">
                                </div>
                                <div class="col-10-12">
                                    <p>
                                        Apart from rent, the following are additional expenses you may incur every month when living in this house.
                                    </p>
                                    <p class="amount">
                                        ₹ 2260.0<b class="red-star">*</b>
                                    </p>
                                    <div id="breakdown-details" class="home-aminities breakdown-details">
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td class="">Electricity(without AC)</td>
                                                    <td class="in-right">₹ 300.0</td>
                                                </tr>
                                                <tr>
                                                    <td class="">Drinking Water</td>
                                                    <td class="in-right">₹ 320.0</td>
                                                </tr>
                                                <tr>
                                                    <td class="">Wifi</td>
                                                    <td class="in-right">₹ 420.0</td>
                                                </tr>
                                                <tr>
                                                    <td class="">Maid/Cleaning</td>
                                                    <td class="in-right">₹ 330.0</td>
                                                </tr>
                                                <tr>
                                                    <td class="">Cook</td>
                                                    <td class="in-right">₹ 830.0</td>
                                                </tr>
                                                <tr>
                                                    <td class="">DTH</td>
                                                    <td class="in-right">₹ 60.0</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <p class="coliving-ps">
                                            <b class="red-star">*</b>The values have been calculated for a single bed in a 3BHK house on twin sharing basis. This is just an indicative figure. Actual costs may vary based on usage.
                                        </p>
                                    </div>
                                    <div>
                                        <button type="button" class="btn btn-sm show-breakdown" id="show-breakdown">+ See Breakdown</button>
                                    </div>
                                </div>
                                <div class="clr"></div>
                            </div>
                        </div> */ ?>
                        <!-- cost of living end-->
                    </div>
                </section>
                <!-- amenities -->
                <section id="amenities" class="nav-section">
                    <div class="box-new home-amenities-widget">
                        <div class="content bootstrap-tab-custom">
                            <h2>Property</h2>
                         <?php /*   <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="ame-tab" class="active"><a href="" aria-controls="living" role="tab" data-toggle="tab" onclick="utils.gaEventTracker(&#39;House Details Page New&#39;, &#39;House Amenities&#39;, &#39;Living Room Tab&#39;);">Living Room</a></li>
                                <li role="ame-tab"><a href="" aria-controls="kitchen" role="tab" data-toggle="tab" onclick="utils.gaEventTracker(&#39;House Details Page New&#39;, &#39;House Amenities&#39;, &#39;Kitchen Tab&#39;);">Kitchen</a></li>
                                <li role="ame-tab"><a href="" aria-controls="bedroom" role="tab" data-toggle="tab" onclick="utils.gaEventTracker(&#39;House Details Page New&#39;, &#39;House Amenities&#39;, &#39;Bed Room Tab&#39;);">Bed Room</a></li>
                                <li role="ame-tab"><a href="" aria-controls="bathroom" role="tab" data-toggle="tab" onclick="utils.gaEventTracker(&#39;House Details Page New&#39;, &#39;House Amenities&#39;, &#39;Bathroom Room Tab&#39;);">Bathroom</a></li>
                            </ul>
                            <!-- Tab panes --> */ ?>
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active animated fadeIn" id="living">
                                    <div class="col-md-12">
                                        <div class="house-img">
                                            <img src="<?php echo base_url('assets/uploads/'.$data[0]->image); ?>">
                                        </div>
                                    </div>
                                  <?php /*  <div class="col-8-12">
                                        <div class="amenities-icon animated fadeInLeft">
                                            <ul>
                                                <li><i class="icon-uniE6CD"></i> Sofa</li>
                                                <li><i class="icon-uniE685"></i> Washing Machine</li>
                                                <li><i class="icon-uniE6E1"></i> WIFI <b class="red-star">*</b></li>
                                                <li><i class="icon-uniE689"></i> Dish TV</li>
                                                <li><i class="icon-uniE6F3"></i> Air Conditioner</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <span class="condition"><b class="red-star">*</b> based on feasibility</span>
                                     */ 
                                    ?>
                                    <div class="clr"></div>
                                </div>
                                <div role="tabpanel" class="tab-pane animated fadeIn" id="kitchen">
                                    <div class="col-4-12">
                                        <div class="house-img">
                                            <img src="<?php echo base_url(); ?>assets/front/detal-vendors/thumb_medium_f712d4d9-1281-4981-94fa-20ad7bf8229b.webp">
                                        </div>
                                    </div>
                                    <div class="col-8-12">
                                        <div class="amenities-icon animated fadeInLeft">
                                            <ul>
                                                <li><i class="icon-uniE656"></i> Dining Table</li>
                                                <li><i class="icon-uniE66B"></i> Fridge</li>
                                                <li><i class="icon-uniE62D"></i> Gas Stove</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="clr"></div>
                                </div>
                                <div role="tabpanel" class="tab-pane animated fadeIn" id="bedroom">
                                    <div class="col-4-12">
                                        <div class="house-img">
                                            <img src="<?php echo base_url(); ?>assets/front/detal-vendors/thumb_medium_df95aa4d-c3ea-4dd8-a8c7-555ad3a1fbe3.webp">
                                        </div>
                                    </div>
                                    <div class="col-8-12">
                                        <div class="amenities-icon animated fadeInLeft">
                                            <ul>
                                                <li><i class="icon-uniE64A"></i> Cupboard</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="clr"></div>
                                </div>
                                <div role="tabpanel" class="tab-pane animated fadeIn" id="bathroom">
                                    <div class="col-4-12">
                                        <div class="house-img">
                                            <img src="<?php echo base_url(); ?>assets/front/detal-vendors/thumb_medium_a8509e71-96b2-401e-afff-c428080613c2.webp">
                                        </div>
                                    </div>
                                    <div class="col-8-12">
                                        <div class="amenities-icon animated fadeInLeft">
                                            <ul>
                                                <li><i class="icon-attached_bathroom"></i> Attached Bathroom</li>
                                                <li><i class="icon-uniE629"></i> Western Toilet</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="clr"></div>
                                </div>
                            </div>
                            <!-- society amenities -->
                        </div>
                    </div>
                </section>
                <!-- amenities -->
                <!-- home details -->
                <section id="house_details" class="nav-section">
                    <div class="box-new house-detail-widget">
                        <div class="content pad-b-zero">
                            <h2>Property Details</h2>
                            <div class="house-detail-widget-sec">
                                <div class="col-1-3">
                                    <span>No. of Bedrooms</span>
                                    <strong><?php echo ($data[0]->no_of_room != '>3') ? $data[0]->no_of_room : 'More than 3';?> BHK</strong>
                                </div>
                                <div class="col-1-3">
                                    <span>Beds</span>
                                    <strong><?php echo ($data[0]->no_of_beds != '>3') ? $data[0]->no_of_beds : 'More than 3';?></strong>
                                </div>
                                <div class="col-1-3">
                                    <span>House Area</span>
                                    <strong><?php echo $this->db->get_where('cities',['id'=> $data[0]->city_id])->result()[0]->city_name; ?></strong>
                                </div>
                            </div>
                            <div class="house-detail-widget-sec">
                                <div class="col-1-3">
                                    <span>Amenities</span>
                                    <strong><?php 
                                $amenities = $this->db->select('amenity_name')->from('amenities')->where_in('id',explode(',',$data[0]->amenities))->get()->result_array();

                                   $amenity = [];
                                  foreach($amenities as $one_amenity)
                                    {                 
                                        $amenity[] = $one_amenity['amenity_name']; 
                                    }  
                                    echo implode(', ',$amenity);
                                ?></strong>
                                </div>
                             <?php /*   <div class="col-1-3">
                                    <span>Food Preference</span>
                                    <strong>None</strong>
                                </div>
                                <div class="col-1-3">
                                    <span>Rent Payment</span>
                                    <strong>Before 5th every month.</strong>
                                </div> */ ?>
                            </div>
                            <?php /* <div class="house-detail-widget-sec">
                                <div class="col-4-12">
                                    <span>Available for</span>
                                    <strong>Boys</strong>
                                </div>
                            </div> */ ?>
                        </div>
                       <?php /*  <div class="house-rules-widget content bootstrap-tab-custom pad-t-zero">
                            <h3>House Rules</h3>
                            <ul class="nav nav-tabs" role="tablist">
                                <div><b>Six Commandments</b></div>
                                <li role="presentation" class="active">
                                    <a href="" aria-controls="home" role="tab" data-toggle="tab">For girls and boys</a>
                                </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content" style="display:block;">
                                <div role="tabpanel" class="tab-pane active" id="home">
                                    <ol>
                                        <li>1. You shall take care of the home as your own</li>
                                        <li>2. You shall look after you roommates like a family</li>
                                        <li>3. You shall be good to neighbours &amp; not wake them up midnight</li>
                                        <li>4. You shall invite us on your birthday</li>
                                        <li>5. You shall party hard but clean up afterwards</li>
                                        <li>6. You shall introduce your roommates to The Big Bang Theory</li>
                                    </ol>
                                </div>
                            </div>
                        </div> */ ?>
                    </div>
                </section>
                <!-- home details -->
                <!-- keep in mind -->
            <?php /*     <section>
                    <div class="box-new">
                        <div class="content things-mind-widget">
                            <h2>Things to keep in mind</h2>
                            <ul>
                                <li>
                                    <div class="col-2-12">
                                        <img alt="Lock-icon" src="<?php echo base_url(); ?>assets/front/detal-vendors/lock-icon-09fad4ccdb893fa744a8dbca10591fa6.svg">
                                    </div>
                                    <div class="col-10-12">
                                        <b>
                    6 months lock-in
                  </b> If you move out before 6 months, you will need to pay a month’s rent as fee.
                                    </div>
                                </li>
                                <li>
                                    <div class="col-2-12">
                                        <img alt="Wifi-icon" src="<?php echo base_url(); ?>assets/front/detal-vendors/wifi-icon-0e0093298d0fb603f47912968c2095e8.svg">
                                    </div>
                                    <div class="col-10-12">
                                        <b>
                    Internet Availability
                  </b> Dependent on feasibility of Internet Service Providers in the area.
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </section> */?>
                <!-- keep in mind -->
            </div>
            <!-- right side widget -->
            <div class="house-book-container">
                <div class="home-book-action-btn sticky-fadein">
                    <div id="tabs-container">
                        <!-- tab kinks -->
                        <ul class="tabs-menu">
                            <li class="current-tab" data-id="tab-1"><a>Visit the Property</a></li>
                           <?php /*  <li data-id="tab-2" id="tab2-booknow"><a href="">Book Now</a></li> */ ?>
                        </ul>
                        <!-- tab kinks -->
                        <!-- tabs -->
                        <div class="tab">
                            <div id="tab-1" class="tab-content2">
                                <div class="side-fix-panel">
                                    <div class="panel-body">
                                        <div itemscope="" itemtype="">
                                            <link href="<?php echo base_url(); ?>assets/front/detal-vendors/schedule_visits-68ec3ea2338803cf85edc0eb0ffc91d5.css" media="all" rel="stylesheet" type="text/css">
                                            
                                            <section class="pre-book place-form new-sav ng-scope" ng-app="naSavApp" ng-controller="savCtrl" ng-init="init(7860)">
                                                <div class="nest-loading ng-hide" ng-show="loading" style="display:block">
                                                    <div class="nest-loading-icon nest-icon-animated nest-icon-rotate">
                                                        <img src="<?php echo base_url(); ?>assets/front/detal-vendors/nest-loading-icn.png" id="animated-icon">
                                                        <span>loading ...</span>
                                                    </div>
                                                </div>
                                                <div class="error-info ng-binding ng-hide" ng-show="serverError.length !== 0">
                                                </div>
                                                <div ng-hide="createdSav">
                                                     <?php 
            if(($this->session->flashdata('message_success'))){
                ?>
                <div class="alert alert-success fade in alert-dismissible" style="margin-top:18px;">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                    <strong>Success!</strong> <?php echo $this->session->flashdata('message_success'); ?>
                </div>
                <?php                
            }
            if(($this->session->flashdata('message_danger'))){
                ?>
                <div class="alert alert-danger  fade in alert-dismissible" style="margin-top:18px;">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                    <strong>Danger!</strong> <?php echo $this->session->flashdata('message_danger'); ?>
                </div>
                <?php                
            }
        ?>
                                                    <form name="userDetails" action="<?php echo base_url('home/Property_interest');?>" method="POST" id="sav-user-details" >
                                                        <div class="field-box">
                                                            <input type="hidden" name="id" value="<?php echo $id;?>">
                                                            <input type="text" placeholder="Name" required="true" id="name" name="name" >
                                                        </div>
                                                        <div class="field-box">
                                                            <input type="number" placeholder="Phone" name="phone" id="phone" required="true">
                                                        </div>
                                                        <div class="field-box">
                                                            <input type="email" placeholder="Email" name="email" id="email" required="true">
                                                        </div>
                                                        <div class="field-box">
                                                            <input type="text" placeholder="Pick a day." ng-model="date" id="datepickerdate" required="required" name="date" >
                                                        </div>
                                                        <div class="field-box">
                                                            <?php /*
                                                            <div class="col-6-12">
                                                                <label>
                                                                    <p>
                                                                        <i class="icon-date"></i> Pick a day
                                                                    </p>
                                                                    <select ng-model="selectedObj.selectedDay" ng-change="updateTimeSlots()" class="select-date ng-pristine ng-untouched ng-valid">
                                                                        <!-- ngRepeat: slot in slots -->
                                                                        <option ng-repeat="slot in slots" class="ng-binding ng-scope" value="Tomorrow">Tomorrow</option>
                                                                        <!-- end ngRepeat: slot in slots -->
                                                                        <option ng-repeat="slot in slots" class="ng-binding ng-scope" value="21st Wed, Feb">21st Wed, Feb</option>
                                                                        <!-- end ngRepeat: slot in slots -->
                                                                        <option ng-repeat="slot in slots" class="ng-binding ng-scope" value="22nd Thu, Feb">22nd Thu, Feb</option>
                                                                        <!-- end ngRepeat: slot in slots -->
                                                                        <option ng-repeat="slot in slots" class="ng-binding ng-scope" value="23rd Fri, Feb">23rd Fri, Feb</option>
                                                                        <!-- end ngRepeat: slot in slots -->
                                                                        <option ng-repeat="slot in slots" class="ng-binding ng-scope" value="24th Sat, Feb">24th Sat, Feb</option>
                                                                        <!-- end ngRepeat: slot in slots -->
                                                                        <option ng-repeat="slot in slots" class="ng-binding ng-scope" value="25th Sun, Feb">25th Sun, Feb</option>
                                                                        <!-- end ngRepeat: slot in slots -->
                                                                    </select> 
                                                                </label>
                                                            </div> 
                                                            <div class="col-6-12">
                                                                <label>
                                                                    <p>
                                                                        <i class="icon-uniE682"></i> Pick a time slot
                                                                    </p>
                                                                    <select ng-model="selectedObj.selectedSlot" ng-change="selectTimeSlot()" class="select-date ng-pristine ng-untouched ng-valid" ng-options="slot.text for slot in timeSlotsObj track by slot.slot_id">
                                                                        <option value="1519041600" selected="selected" label="05:30 PM">05:30 PM</option>
                                                                        <option value="1519047000" label="07:00 PM">07:00 PM</option>
                                                                        <option value="1519048800" label="07:30 PM">07:30 PM</option>
                                                                        <option value="1519050600" label="08:00 PM">08:00 PM</option>
                                                                        <option value="1519052400" label="08:30 PM">08:30 PM</option>
                                                                    </select>
                                                                </label>
                                                            </div> */ ?>
                                                            <div class="clr"></div>
                                                        </div> 
                                                        <div class="clr"></div>
                                                        <?php /*
                                                        <div class="field-box referral-wrapper">
                                                            <input type="text" placeholder="Referral Code (if any)" ng-model="referralCode" ng-pattern="/^[a-zA-Z0-9]{6,10}$/" name="referralCode" ng-change="updateReferralCodeErrorMessage()" ng-click="showPopover()" class="ng-pristine ng-untouched ng-valid ng-valid-pattern">
                                                            <button class="btn referral-apply" ng-click="validateReferralCode($event)" ng-disabled="(userDetails.referralCode.$invalid ||
          userDetails.referralCode.$pristine || disableVerifyBtn)" ng-class="{&#39;disabled&#39;: (userDetails.referralCode.$invalid &amp;&amp;
          !userDetails.referralCode.$pristine)}" disabled="disabled">Verify</button>
                                                            <p ng-class="{&#39;error-info&#39;: !validReferralCode, &#39;success-info&#39;: validReferralCode }" ng-show="(userDetails.referralCode.$invalid &amp;&amp; !userDetails.referralCode.$pristine) || referralCodeMsg.length &gt; 0" class="ng-binding ng-hide error-info">
                                                                Please enter a valid referral code
                                                            </p>
                                                            <a href="" id="referral-tooltip" class="referral-tooltip" data-toggle="popover" data-placement="bottom" data-trigger="hover touch" data-content="Enter the referral code you got from your friend to get INR 500 off your first month&#39;s rent. Don&#39;t have one? Ask your friends for their Nestaway referral code." data-original-title="" title=""><img alt="Nestaway info icon" src="<?php echo base_url(); ?>assets/front/detal-vendors/info-icn-black-a3c1ecc33f4c27a85d9358d6982175e6.svg"></a>
                                                        </div>
                                                         */ ?>
                                                        <div class="btns-wrapper">
                                                            <div class="sav-btn-wrapper" onclick="utils.gaEventTracker(&#39;House Details Page New&#39;, &#39;Schedule Sidebar&#39;, &#39;SAV button&#39;);">
                                                                <!-- ngIf: slots.length > 0 -->
                                                                <input type="submit" class="btn btn-primary btn-block ng-scope" ng-click="setSavSlot()" value="Schedule A Visit" ng-if="slots.length &gt; 0">
                                                                <!-- end ngIf: slots.length > 0 -->
                                                                <abbr class="nestaway-tooltp sav-tooltip" rel="tooltip" title="You can take a free tour of the house before booking. Pick a date and an available time slot for visit. Our representative will be there to show you the house." ng-show="tooltip">
                                                                    <img ng-src="/assets/house-detail/info-icn.svg" src="<?php echo base_url(); ?>assets/front/detal-vendors/info-icn.svg">
                                                                </abbr>
                                                            </div>
                                                            <div class="connect-tenant-wrapper slot-info ng-hide" ng-show="connectTenant">
                                                                <span class="or">OR</span>
                                                                <p ng-bind-html="connectTenant.msg" class="sav"></p>
                                                                <input type="submit" id="connectTenant" ng-click="setSavSlot(true)" value="" class="btn connect-btn">
                                                                <abbr class="nestaway-tooltp sav-tooltip ng-hide" rel="tooltip" title="" ng-show="connectTenant.tooltip">i</abbr>
                                                            </div>
                                                        </div>
                                                    </form>
                                                    <!-- scheduled info -->
                                                   <?php /*  <div class="sav-info-wrapper">
                                                        <div class="col-2-12 pad-r-zero">
                                                            <img alt="Hurry-icon" src="<?php echo base_url(); ?>assets/front/detal-vendors/hurry-icon-0cc094fab25acaf60878eba42af3ac4f.svg">
                                                        </div>
                                                       <div class="col-10-12">
                                                            <b>5 people scheduled visit</b>.
                                                            <span><a id="book-now">Book Now</a> to avoid missing out.</span>
                                                        </div> 
                                                        <div class="clr"></div>
                                                    </div> */?>
                                                    <!-- scheduled info -->
                                                </div>
                                                <div class="step-3 ng-hide" ng-show="createdSav &amp;&amp; showOtp">
                                                    <div class="otp-info">
                                                        <p class="ng-binding"></p>
                                                        <div class="field-box">
                                                            <input type="text" ng-model="otp" placeholder="Enter OTP sent to your phone" required="" class="ng-pristine ng-untouched ng-invalid ng-invalid-required">
                                                        </div>
                                                        <p class="otp-msg">
                                                            Didn't receive OTP? <a class="resend-otp" ng-click="createSav(true)">Resend OTP</a>
                                                        </p>
                                                        <div class="btns-wrapper">
                                                            <button class="btn back-btn" ng-click="previousStep()">Back</button>
                                                            <button class="btn continue-btn" ng-click="verifyAndSchedule()">Verify and Schedule</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="step-3 success-wrapper ng-hide" ng-show="createdSav &amp;&amp; !showOtp">
                                                    <p class="title ng-binding">
                                                    </p>
                                                    <p class="ng-binding">
                                                    </p>
                                                    <div class="info ">
                                                        <p class="ng-binding"></p>
                                                        <p>
                                                            <b class="ng-binding"> at </b>
                                                        </p>
                                                    </div>
                                                    <div class="sav-recommendations-section ng-hide" ng-show="savRecommendationHouses.length &gt; 0">
                                                        <div class="sav-recommendation-label">
                                                            <b> Houses available nearby for a visit </b>
                                                        </div>
                                                        <div class="owl-carousel" id="sav-carousel">
                                                            <!-- ngRepeat: house in savRecommendationHouses -->
                                                        </div>
                                                    </div>
                                                    <!-- Resultrix tracking, only in production. -->
                                                    <div style="display:inline;" id="resultrix-pixel"><img height="1" width="1" style="border-style:none;" alt=""></div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-2" class="tab-content2">
                                <!--==== fixed details ====-->
                                <div class="side-fix-panel schedule-side-fix-panel">
                                    <div class="pre-book panel-body">
                                        <div class="clr"></div>
                                        <div id="form_error_div2" class="error" style="display:none;  font-size: 12px;"></div>
                                        <input type="hidden" value="false" id="profile-complete">
                                        <section class="pre-book place-form" style="padding-top: 0">
                                            <div class="float-animate">
                                                <p class="pay-info-text">
                                                    To Book, you need to pay a token amount now.
                                                    <br> And the remaining before move-in.
                                                </p>
                                                <button id="book-now-login" class="btn btn-primary btn-block" onclick="utils.gaEventTracker(&#39;House Details Page New&#39;, &#39;Book Now Sidebar&#39;, &#39;Pay Token button&#39;);">Pay Token Amount</button>
                                            </div>
                                            <div class="cancellation-policy" data-toggle="modal" data-target="#cancellation-policy">Cancellation Policy *</div>
                                        </section>
                                    </div>
                                </div>
                                <div class="normal-plain-popup" id="addnl-cost">
                                    <div class="normal-popup-widget book-instruction">
                                        <span id="movein-cancel-btn" href="javascript:void(0)">
          <i class="icon-uniE6EF"></i>
      </span>
                                        <h2 style="color: #fa7683 ; margin-bottom:10px; width: 50%;">ADDITIONAL CHARGES</h2>
                                        <div class="overflow-content" style="height: auto;">
                                            <div class="overflow-content-inside">
                                                <ol>
                                                    <li>To cover the expenses of stamp duty, police verification, agreement printing, notary charges, etc, a one-time onboarding charge will be applicable at the time of Security Deposit Payment</li>
                                                    <li>Additional cancellation charges would be applicable for Delhi as per <a href="" target="_blank">Terms &amp; conditions</a>.</li>
                                                </ol>
                                                <button type="submit" class="red-btn-nest" onclick="submitFormUponAddnlCostOK();return false;">OK</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <!-- tabs -->
                    </div>
                    <div class="clr"></div>
                    <?php /*
                    <!-- other info -->
                    <div class="info-wrapper">
                        <div class="col-2-12 pad-r-zero">
                            <img alt="Cash-icon" src="<?php echo base_url(); ?>assets/front/detal-vendors/cash-icon-236e656f1c70f46ac40d73530b0ef862.svg">
                        </div>
                        <div class="col-10-12 pad-r-zero">
                            <b>3 Days Trial</b>
                            <p>
                                If you don’t like the house within 3-days of move-in, you can get a refund.
                                <a id="money-back" href="" target="_blank">More Here</a>
                            </p>
                        </div>
                        <div class="clr"></div>
                    </div> */?>
                    <!-- other info -->
                </div>
            </div>
            <div class="clr"></div>
        </div>
        <?php /*
        <div class="near-by-widget nav-section" id="nearby">
            <div class="grid">
                <!-- near by -->
                <section>
                    <link href="<?php echo base_url(); ?>assets/front/detal-vendors/nearby-00dd5798fbae24f65a9a15bd1f7d0ce1.css" media="all" rel="stylesheet" type="text/css">
                    <!-- on the map -->
                    <div id="house-nearby" ng-app="nearBy" ng-controller="nearByCtrl" class="ng-scope">
                        <h2 ng-show="showNearby">The Neighborhood</h2>
                        <div ng-show="showNearby" class="grid-card-box map-nearby-grid" ng-init="initializeHouse({id: 7860, location: {latitude: 28.5743745, longitude: 77.2564825}, title: &quot;Nathu Nest Delhi&quot;, address: &quot;40, siddhartha enclave, new delhi, delhi, india, delhi- 110014&quot;})">
                            <div class="map-nearby-widget">
                                <ul>
                                    <!-- ngRepeat: item in nearbyPlaces -->
                                    <li class="active-panel-tab" ng-repeat="item in nearbyPlaces">
                                        <span ng-click="addPlacesMarker(item.category)" class="ng-binding">Commute</span>
                                        <div class="tab-panel-widget active">
                                            <!-- ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Ashram</strong>
                                                <span class="ng-binding">0.8 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Hazrat Nizamuddin</strong>
                                                <span class="ng-binding">2.0 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Ishwar Nagar</strong>
                                                <span class="ng-binding">2.4 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Okhla</strong>
                                                <span class="ng-binding">2.4 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Sarai Kale Khan</strong>
                                                <span class="ng-binding">3.0 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Sukhdev Vihar Depot</strong>
                                                <span class="ng-binding">3.0 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Lajpat Nagar</strong>
                                                <span class="ng-binding">3.8 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Sukhdev Vihar Bus Stop</strong>
                                                <span class="ng-binding">4.0 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                        </div>
                                    </li>
                                    <!-- end ngRepeat: item in nearbyPlaces -->
                                    <li class="" ng-repeat="item in nearbyPlaces">
                                        <span ng-click="addPlacesMarker(item.category)" class="ng-binding">Restaurants &amp; Bars</span>
                                        <div class="tab-panel-widget ">
                                            <!-- ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Janta Bakery</strong>
                                                <span class="ng-binding">1.5 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Mughal Restaurant</strong>
                                                <span class="ng-binding">2.4 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Mughal Restaurant</strong>
                                                <span class="ng-binding">2.4 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Nirula's</strong>
                                                <span class="ng-binding">2.4 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Domino's</strong>
                                                <span class="ng-binding">2.4 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Club BW</strong>
                                                <span class="ng-binding">2.5 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Domino's Pizza</strong>
                                                <span class="ng-binding">2.5 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Sagar Ratana</strong>
                                                <span class="ng-binding">2.5 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Moti Mahal Delux Tandoori Trail</strong>
                                                <span class="ng-binding">2.7 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Sachdeva Bakers</strong>
                                                <span class="ng-binding">2.9 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Indian Accent</strong>
                                                <span class="ng-binding">3.0 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Nirula's</strong>
                                                <span class="ng-binding">3.0 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Mr. Deliciousness Delivered</strong>
                                                <span class="ng-binding">3.7 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Ek Bar</strong>
                                                <span class="ng-binding">4.5 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Cafe Turtle</strong>
                                                <span class="ng-binding">4.8 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                        </div>
                                    </li>
                                    <!-- end ngRepeat: item in nearbyPlaces -->
                                    <li class="" ng-repeat="item in nearbyPlaces">
                                        <span ng-click="addPlacesMarker(item.category)" class="ng-binding">Health Care</span>
                                        <div class="tab-panel-widget ">
                                            <!-- ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Apollo PHARMACY</strong>
                                                <span class="ng-binding">1.4 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Apollo Pharmacy</strong>
                                                <span class="ng-binding">1.5 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Vimhans PrimaMed Super Speciality Hospital - Best Hospital in India</strong>
                                                <span class="ng-binding">2.0 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Loyal Pharmacy</strong>
                                                <span class="ng-binding">2.6 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Apollo Pharmacy</strong>
                                                <span class="ng-binding">2.9 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">98.4 Your Chemist For Life</strong>
                                                <span class="ng-binding">2.9 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Dr. Harbhajan Singh</strong>
                                                <span class="ng-binding">2.9 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Advanced Fertility and Gynecology Centre</strong>
                                                <span class="ng-binding">3.1 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Navya Ent &amp; Gynae Clinic</strong>
                                                <span class="ng-binding">3.3 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Holy Family Hospital</strong>
                                                <span class="ng-binding">3.4 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Lions Kidney Hospitals</strong>
                                                <span class="ng-binding">3.5 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Dr Lal PathLabs</strong>
                                                <span class="ng-binding">4.4 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                        </div>
                                    </li>
                                    <!-- end ngRepeat: item in nearbyPlaces -->
                                    <li class="" ng-repeat="item in nearbyPlaces">
                                        <span ng-click="addPlacesMarker(item.category)" class="ng-binding">Education</span>
                                        <div class="tab-panel-widget ">
                                            <!-- ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Dev Samaj Modern School</strong>
                                                <span class="ng-binding">1.8 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Madaan Book Store</strong>
                                                <span class="ng-binding">1.9 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Public Library</strong>
                                                <span class="ng-binding">2.4 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Ravi Book Store</strong>
                                                <span class="ng-binding">2.9 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Jamia Millia Islamia</strong>
                                                <span class="ng-binding">3.6 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Public Library</strong>
                                                <span class="ng-binding">4.4 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                        </div>
                                    </li>
                                    <!-- end ngRepeat: item in nearbyPlaces -->
                                    <li class="" ng-repeat="item in nearbyPlaces">
                                        <span ng-click="addPlacesMarker(item.category)" class="ng-binding">Shopping</span>
                                        <div class="tab-panel-widget ">
                                            <!-- ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Nitin Departmental Store</strong>
                                                <span class="ng-binding">0.9 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Twenty Four Seven</strong>
                                                <span class="ng-binding">1.3 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Morning Store</strong>
                                                <span class="ng-binding">2.9 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Super Bazar</strong>
                                                <span class="ng-binding">3.0 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Grocery Home Delivery Delhi</strong>
                                                <span class="ng-binding">3.1 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Reliance Fresh</strong>
                                                <span class="ng-binding">4.2 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Ansal Plaza</strong>
                                                <span class="ng-binding">4.6 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Archana Shopping Complex</strong>
                                                <span class="ng-binding">4.9 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                        </div>
                                    </li>
                                    <!-- end ngRepeat: item in nearbyPlaces -->
                                    <li class="" ng-repeat="item in nearbyPlaces">
                                        <span ng-click="addPlacesMarker(item.category)" class="ng-binding">Personal Care</span>
                                        <div class="tab-panel-widget ">
                                            <!-- ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Gold's Gym</strong>
                                                <span class="ng-binding">1.4 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">The Leather Laundry (Delhi,Gurgaon)-Leather Bags, Shoes, Jackets, Belts, Sofa- Dry Clean &amp; Repair</strong>
                                                <span class="ng-binding">1.5 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Eves Beauty Parlour and Academy</strong>
                                                <span class="ng-binding">2.4 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Jawed Habib Hair &amp; Beauty Salon</strong>
                                                <span class="ng-binding">2.6 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Blush Beauty Salon</strong>
                                                <span class="ng-binding">2.7 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Quick clean Laundromat</strong>
                                                <span class="ng-binding">2.7 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Join The Gym</strong>
                                                <span class="ng-binding">2.8 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Blue Gym</strong>
                                                <span class="ng-binding">3.0 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Oranz Body Spa</strong>
                                                <span class="ng-binding">3.0 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Oasis Spa</strong>
                                                <span class="ng-binding">3.1 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                        </div>
                                    </li>
                                    <!-- end ngRepeat: item in nearbyPlaces -->
                                    <li class="" ng-repeat="item in nearbyPlaces">
                                        <span ng-click="addPlacesMarker(item.category)" class="ng-binding">Basic Amenities</span>
                                        <div class="tab-panel-widget ">
                                            <!-- ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Indicash ATM</strong>
                                                <span class="ng-binding">0.9 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Badal Movie Theater Pvt</strong>
                                                <span class="ng-binding">1.0 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Indian Oil Fuel Station</strong>
                                                <span class="ng-binding">1.2 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Citi Bank ATM</strong>
                                                <span class="ng-binding">2.3 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Glitz Cinemas</strong>
                                                <span class="ng-binding">2.3 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Citi Bank ATM</strong>
                                                <span class="ng-binding">2.6 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Citi Bank ATM - Lajpat Nagar II</strong>
                                                <span class="ng-binding">2.8 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">PVR 3C's</strong>
                                                <span class="ng-binding">2.9 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Competent Cine Court</strong>
                                                <span class="ng-binding">2.9 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Bank of India</strong>
                                                <span class="ng-binding">2.9 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Indicash Atm</strong>
                                                <span class="ng-binding">3.6 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Indian Bank</strong>
                                                <span class="ng-binding">3.7 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">HP Fuel Station</strong>
                                                <span class="ng-binding">4.2 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Indian Bank</strong>
                                                <span class="ng-binding">4.7 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                            <div ng-repeat="place in item.data" class="tab-panel-item ng-scope" ng-click="showDirections(place)">
                                                <strong class="ellipsify ng-binding">Indian Oil Petrol Pump</strong>
                                                <span class="ng-binding">4.9 km</span>
                                            </div>
                                            <!-- end ngRepeat: place in item.data -->
                                        </div>
                                    </li>
                                    <!-- end ngRepeat: item in nearbyPlaces -->
                                </ul>
                            </div>
                            <div id="map_canvas" style="width:75%;float:right;">
                                <ui-gmap-google-map center="map.center" zoom="map.zoom" draggable="true" options="map.options" control="map.mapControl" class="ng-isolate-scope angular-google-map">
                                    <div class="angular-google-map">
                                        <div class="angular-google-map-container" style="position: relative; overflow: hidden;">
                                            <div style="height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; background-color: rgb(229, 227, 223);">
                                                <div class="gm-style" style="position: absolute; z-index: 0; left: 0px; top: 0px; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px;">
                                                    <div tabindex="0" style="position: absolute; z-index: 0; left: 0px; top: 0px; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px;">
                                                        <div style="z-index: 1; position: absolute; left: 50%; top: 50%; transform: translate(0px, 0px);">
                                                            <div style="position: absolute; left: 0px; top: 0px; z-index: 100; width: 100%;">
                                                                <div style="position: absolute; left: 0px; top: 0px; z-index: 0;">
                                                                    <div style="position: absolute; z-index: 1; transform: matrix(1, 0, 0, 1, -12, -23);">
                                                                        <div style="width: 256px; height: 256px; position: absolute; left: 0px; top: 0px;"></div>
                                                                        <div style="width: 256px; height: 256px; position: absolute; left: -256px; top: 0px;"></div>
                                                                        <div style="width: 256px; height: 256px; position: absolute; left: -256px; top: -256px;"></div>
                                                                        <div style="width: 256px; height: 256px; position: absolute; left: 0px; top: -256px;"></div>
                                                                        <div style="width: 256px; height: 256px; position: absolute; left: 256px; top: -256px;"></div>
                                                                        <div style="width: 256px; height: 256px; position: absolute; left: 256px; top: 0px;"></div>
                                                                        <div style="width: 256px; height: 256px; position: absolute; left: 256px; top: 256px;"></div>
                                                                        <div style="width: 256px; height: 256px; position: absolute; left: 0px; top: 256px;"></div>
                                                                        <div style="width: 256px; height: 256px; position: absolute; left: -256px; top: 256px;"></div>
                                                                        <div style="width: 256px; height: 256px; position: absolute; left: -512px; top: 256px;"></div>
                                                                        <div style="width: 256px; height: 256px; position: absolute; left: -512px; top: 0px;"></div>
                                                                        <div style="width: 256px; height: 256px; position: absolute; left: -512px; top: -256px;"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div style="position: absolute; left: 0px; top: 0px; z-index: 101; width: 100%;"></div>
                                                            <div style="position: absolute; left: 0px; top: 0px; z-index: 102; width: 100%;"></div>
                                                            <div style="position: absolute; left: 0px; top: 0px; z-index: 103; width: 100%;">
                                                                <div style="position: absolute; left: 0px; top: 0px; z-index: -1;">
                                                                    <div style="position: absolute; z-index: 1; transform: matrix(1, 0, 0, 1, -12, -23);">
                                                                        <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 0px; top: 0px;"></div>
                                                                        <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -256px; top: 0px;"></div>
                                                                        <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -256px; top: -256px;"></div>
                                                                        <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 0px; top: -256px;"></div>
                                                                        <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 256px; top: -256px;"></div>
                                                                        <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 256px; top: 0px;"></div>
                                                                        <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 256px; top: 256px;"></div>
                                                                        <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 0px; top: 256px;"></div>
                                                                        <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -256px; top: 256px;"></div>
                                                                        <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -512px; top: 256px;"></div>
                                                                        <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -512px; top: 0px;"></div>
                                                                        <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -512px; top: -256px;"></div>
                                                                    </div>
                                                                </div>
                                                                <div style="width: 32px; height: 53px; overflow: hidden; position: absolute; left: -25px; top: -72px; z-index: -19;"><img alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/map_icon.png" draggable="false" style="position: absolute; left: 0px; top: 0px; user-select: none; width: 32px; height: 53px; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                <div style="width: 28px; height: 42px; overflow: hidden; position: absolute; left: -8px; top: -45px; z-index: -3;"><img alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/bus_station.png" draggable="false" style="position: absolute; left: 0px; top: 0px; user-select: none; width: 28px; height: 42px; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                <div style="width: 28px; height: 42px; overflow: hidden; position: absolute; left: 34px; top: 24px; z-index: 66;"><img alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/bus_station.png" draggable="false" style="position: absolute; left: 0px; top: 0px; user-select: none; width: 28px; height: 42px; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                <div style="width: 28px; height: 42px; overflow: hidden; position: absolute; left: -8px; top: -145px; z-index: -103;"><img alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/bus_station.png" draggable="false" style="position: absolute; left: 0px; top: 0px; user-select: none; width: 28px; height: 42px; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                <div style="width: 28px; height: 42px; overflow: hidden; position: absolute; left: 64px; top: 68px; z-index: 110;"><img alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/bus_station.png" draggable="false" style="position: absolute; left: 0px; top: 0px; user-select: none; width: 28px; height: 42px; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                <div style="width: 28px; height: 42px; overflow: hidden; position: absolute; left: 67px; top: 74px; z-index: 116;"><img alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/bus_station.png" draggable="false" style="position: absolute; left: 0px; top: 0px; user-select: none; width: 28px; height: 42px; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                <div style="width: 28px; height: 42px; overflow: hidden; position: absolute; left: -40px; top: -158px; z-index: -116;"><img alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/train_station.png" draggable="false" style="position: absolute; left: 0px; top: 0px; user-select: none; width: 28px; height: 42px; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                <div style="width: 28px; height: 42px; overflow: hidden; position: absolute; left: 31px; top: 37px; z-index: 79;"><img alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/train_station.png" draggable="false" style="position: absolute; left: 0px; top: 0px; user-select: none; width: 28px; height: 42px; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                <div style="width: 28px; height: 42px; overflow: hidden; position: absolute; left: -95px; top: -91px; z-index: -49;"><img alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/train_station.png" draggable="false" style="position: absolute; left: 0px; top: 0px; user-select: none; width: 28px; height: 42px; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                            </div>
                                                            <div style="position: absolute; left: 0px; top: 0px; z-index: 0;">
                                                                <div style="position: absolute; z-index: 1; transform: matrix(1, 0, 0, 1, -12, -23);">
                                                                    <div style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px;"><img draggable="false" alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/vt" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                    <div style="position: absolute; left: -256px; top: 0px; width: 256px; height: 256px;"><img draggable="false" alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/vt(1)" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                    <div style="position: absolute; left: -256px; top: -256px; width: 256px; height: 256px;"><img draggable="false" alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/vt(2)" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                    <div style="position: absolute; left: 0px; top: -256px; width: 256px; height: 256px;"><img draggable="false" alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/vt(3)" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                    <div style="position: absolute; left: 256px; top: -256px; width: 256px; height: 256px;"><img draggable="false" alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/vt(4)" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                    <div style="position: absolute; left: 256px; top: 0px; width: 256px; height: 256px;"><img draggable="false" alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/vt(5)" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                    <div style="position: absolute; left: 256px; top: 256px; width: 256px; height: 256px;"><img draggable="false" alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/vt(6)" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                    <div style="position: absolute; left: 0px; top: 256px; width: 256px; height: 256px;"><img draggable="false" alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/vt(7)" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                    <div style="position: absolute; left: -256px; top: 256px; width: 256px; height: 256px;"><img draggable="false" alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/vt(8)" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                    <div style="position: absolute; left: -512px; top: 256px; width: 256px; height: 256px;"><img draggable="false" alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/vt(9)" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                    <div style="position: absolute; left: -512px; top: 0px; width: 256px; height: 256px;"><img draggable="false" alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/vt(10)" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                    <div style="position: absolute; left: -512px; top: -256px; width: 256px; height: 256px;"><img draggable="false" alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/vt(11)" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="gm-style-pbc" style="z-index: 2; position: absolute; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; left: 0px; top: 0px; opacity: 0;">
                                                            <p class="gm-style-pbt"></p>
                                                        </div>
                                                        <div style="z-index: 3; position: absolute; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; left: 0px; top: 0px;">
                                                            <div style="z-index: 1; position: absolute; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; left: 0px; top: 0px;"></div>
                                                            <div style="z-index: 4; position: absolute; left: 50%; top: 50%; transform: translate(0px, 0px);">
                                                                <div style="position: absolute; left: 0px; top: 0px; z-index: 104; width: 100%;"></div>
                                                                <div style="position: absolute; left: 0px; top: 0px; z-index: 105; width: 100%;"></div>
                                                                <div style="position: absolute; left: 0px; top: 0px; z-index: 106; width: 100%;">
                                                                    <div style="width: 48px; height: 69px; overflow: hidden; position: absolute; opacity: 0.01; cursor: pointer; left: -33px; top: -80px; z-index: -19;" class="gmnoprint" title=""><img alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/map_icon.png" draggable="false" style="position: absolute; left: 0px; top: 0px; width: 48px; height: 69px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                    <div style="width: 44px; height: 58px; overflow: hidden; position: absolute; opacity: 0.01; cursor: pointer; left: -16px; top: -53px; z-index: -3;" class="gmnoprint" title=""><img alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/bus_station.png" draggable="false" style="position: absolute; left: 0px; top: 0px; width: 44px; height: 58px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                    <div style="width: 44px; height: 58px; overflow: hidden; position: absolute; opacity: 0.01; cursor: pointer; left: 26px; top: 16px; z-index: 66;" class="gmnoprint" title=""><img alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/bus_station.png" draggable="false" style="position: absolute; left: 0px; top: 0px; width: 44px; height: 58px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                    <div style="width: 44px; height: 58px; overflow: hidden; position: absolute; opacity: 0.01; cursor: pointer; left: -16px; top: -153px; z-index: -103;" class="gmnoprint" title=""><img alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/bus_station.png" draggable="false" style="position: absolute; left: 0px; top: 0px; width: 44px; height: 58px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                    <div style="width: 44px; height: 58px; overflow: hidden; position: absolute; opacity: 0.01; cursor: pointer; left: 56px; top: 60px; z-index: 110;" class="gmnoprint" title=""><img alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/bus_station.png" draggable="false" style="position: absolute; left: 0px; top: 0px; width: 44px; height: 58px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                    <div style="width: 44px; height: 58px; overflow: hidden; position: absolute; opacity: 0.01; cursor: pointer; left: 59px; top: 66px; z-index: 116;" class="gmnoprint" title=""><img alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/bus_station.png" draggable="false" style="position: absolute; left: 0px; top: 0px; width: 44px; height: 58px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                    <div style="width: 44px; height: 58px; overflow: hidden; position: absolute; opacity: 0.01; cursor: pointer; left: -48px; top: -166px; z-index: -116;" class="gmnoprint" title=""><img alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/train_station.png" draggable="false" style="position: absolute; left: 0px; top: 0px; width: 44px; height: 58px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                    <div style="width: 44px; height: 58px; overflow: hidden; position: absolute; opacity: 0.01; cursor: pointer; left: 23px; top: 29px; z-index: 79;" class="gmnoprint" title=""><img alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/train_station.png" draggable="false" style="position: absolute; left: 0px; top: 0px; width: 44px; height: 58px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                    <div style="width: 44px; height: 58px; overflow: hidden; position: absolute; opacity: 0.01; cursor: pointer; left: -103px; top: -99px; z-index: -49;" class="gmnoprint" title=""><img alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/train_station.png" draggable="false" style="position: absolute; left: 0px; top: 0px; width: 44px; height: 58px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                </div>
                                                                <div style="position: absolute; left: 0px; top: 0px; z-index: 107; width: 100%;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <iframe frameborder="0" src="<?php echo base_url(); ?>assets/front/detal-vendors/saved_resource.html" style="z-index: -1; position: absolute; width: 100%; height: 100%; top: 0px; left: 0px; border: none;"></iframe>
                                                    <div style="margin-left: 5px; margin-right: 5px; z-index: 1000000; position: absolute; left: 0px; bottom: 0px;">
                                                        <a target="_blank" href="" title="Click to see this area on Google Maps" style="position: static; overflow: visible; float: none; display: inline;">
                                                            <div style="width: 66px; height: 26px; cursor: pointer;"><img alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/google4.png" draggable="false" style="position: absolute; left: 0px; top: 0px; width: 66px; height: 26px; user-select: none; border: 0px; padding: 0px; margin: 0px;"></div>
                                                        </a>
                                                    </div>
                                                    <div style="background-color: white; padding: 15px 21px; border: 1px solid rgb(171, 171, 171); font-family: Roboto, Arial, sans-serif; color: rgb(34, 34, 34); box-shadow: rgba(0, 0, 0, 0.2) 0px 4px 16px; z-index: 10000002; display: none; width: 256px; height: 148px; position: absolute; left: 230px; top: 160px;">
                                                        <div style="padding: 0px 0px 10px; font-size: 16px;">Map Data</div>
                                                        <div style="font-size: 13px;">Map data ©2018 Google</div>
                                                        <div style="width: 13px; height: 13px; overflow: hidden; position: absolute; opacity: 0.7; right: 12px; top: 12px; z-index: 10000;"><img alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/mapcnt6.png" draggable="false" style="position: absolute; left: -2px; top: -336px; width: 59px; height: 492px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><img alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/transparent.png" draggable="false" style="width: 37px; height: 37px; user-select: none; border: 0px; padding: 0px; margin: 0px; position: absolute; right: 0px; top: 0px; z-index: 10001; cursor: pointer;"></div>
                                                    <div class="gmnoprint" style="z-index: 1000001; position: absolute; right: 72px; bottom: 0px; width: 121px;">
                                                        <div draggable="false" class="gm-style-cc" style="user-select: none; height: 14px; line-height: 14px;">
                                                            <div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;">
                                                                <div style="width: 1px;"></div>
                                                                <div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div>
                                                            </div>
                                                            <div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a style="text-decoration: none; cursor: pointer; display: none;">Map Data</a><span>Map data ©2018 Google</span></div>
                                                        </div>
                                                    </div>
                                                    <div class="gmnoscreen" style="position: absolute; right: 0px; bottom: 0px;">
                                                        <div style="font-family: Roboto, Arial, sans-serif; font-size: 11px; color: rgb(68, 68, 68); direction: ltr; text-align: right; background-color: rgb(245, 245, 245);">Map data ©2018 Google</div>
                                                    </div>
                                                    <div class="gmnoprint gm-style-cc" draggable="false" style="z-index: 1000001; user-select: none; height: 14px; line-height: 14px; position: absolute; right: 0px; bottom: 0px;">
                                                        <div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;">
                                                            <div style="width: 1px;"></div>
                                                            <div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div>
                                                        </div>
                                                        <div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a href="" target="_blank" style="text-decoration: none; cursor: pointer; color: rgb(68, 68, 68);">Terms of Use</a></div>
                                                    </div>
                                                    <button draggable="false" title="Toggle fullscreen view" aria-label="Toggle fullscreen view" type="button" style="background: none; border: 0px; margin: 10px 14px; padding: 0px; position: absolute; cursor: pointer; user-select: none; width: 25px; height: 25px; overflow: hidden; top: 0px; right: 0px;"><img alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/sv9.png" draggable="false" class="gm-fullscreen-control" style="position: absolute; left: -52px; top: -86px; width: 164px; height: 175px; user-select: none; border: 0px; padding: 0px; margin: 0px;"></button>
                                                    <div class="gmnoprint gm-bundled-control gm-bundled-control-on-bottom" draggable="false" controlwidth="28" controlheight="93" style="margin: 10px; user-select: none; position: absolute; bottom: 107px; right: 28px;">
                                                        <div class="gmnoprint" controlwidth="28" controlheight="55" style="position: absolute; left: 0px; top: 38px;">
                                                            <div draggable="false" style="user-select: none; box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px; border-radius: 2px; cursor: pointer; background-color: rgb(255, 255, 255); width: 28px; height: 55px;">
                                                                <button draggable="false" title="Zoom in" aria-label="Zoom in" type="button" style="background: none; display: block; border: 0px; margin: 0px; padding: 0px; position: relative; cursor: pointer; user-select: none; width: 28px; height: 27px; top: 0px; left: 0px;">
                                                                    <div style="overflow: hidden; position: absolute; width: 15px; height: 15px; left: 7px; top: 6px;"><img alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/tmapctrl.png" draggable="false" style="position: absolute; left: 0px; top: 0px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 120px; height: 54px;"></div>
                                                                </button>
                                                                <div style="position: relative; overflow: hidden; width: 67%; height: 1px; left: 16%; background-color: rgb(230, 230, 230); top: 0px;"></div>
                                                                <button draggable="false" title="Zoom out" aria-label="Zoom out" type="button" style="background: none; display: block; border: 0px; margin: 0px; padding: 0px; position: relative; cursor: pointer; user-select: none; width: 28px; height: 27px; top: 0px; left: 0px;">
                                                                    <div style="overflow: hidden; position: absolute; width: 15px; height: 15px; left: 7px; top: 6px;"><img alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/tmapctrl.png" draggable="false" style="position: absolute; left: 0px; top: -15px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 120px; height: 54px;"></div>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="gm-svpc" controlwidth="28" controlheight="28" style="background-color: rgb(255, 255, 255); box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px; border-radius: 2px; width: 28px; height: 28px; default; position: absolute; left: 0px; top: 0px;">
                                                            <div style="position: absolute; left: 1px; top: 1px;"></div>
                                                            <div style="position: absolute; left: 1px; top: 1px;">
                                                                <div aria-label="Street View Pegman Control" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px;"><img alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/cb_scout5.png" draggable="false" style="position: absolute; left: -147px; top: -26px; width: 215px; height: 835px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                <div aria-label="Pegman is on top of the Map" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px; visibility: hidden;"><img alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/cb_scout5.png" draggable="false" style="position: absolute; left: -147px; top: -52px; width: 215px; height: 835px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                                <div aria-label="Street View Pegman Control" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px; visibility: hidden;"><img alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/cb_scout5.png" draggable="false" style="position: absolute; left: -147px; top: -78px; width: 215px; height: 835px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="gmnoprint gm-bundled-control gm-bundled-control-on-bottom" draggable="false" controlwidth="0" controlheight="0" style="margin: 10px; user-select: none; position: absolute; display: none; bottom: 26px; left: 0px;">
                                                        <div class="gmnoprint" controlwidth="28" controlheight="0" style="display: none; position: absolute;">
                                                            <div title="Rotate map 90 degrees" style="width: 28px; height: 28px; overflow: hidden; position: absolute; background-color: rgb(255, 255, 255); box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px; border-radius: 2px; cursor: pointer; display: none;"><img alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/tmapctrl4.png" draggable="false" style="position: absolute; left: -141px; top: 6px; width: 170px; height: 54px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                            <div class="gm-tilt" style="width: 28px; height: 28px; overflow: hidden; position: absolute; background-color: rgb(255, 255, 255); box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px; border-radius: 2px; top: 0px; cursor: pointer;"><img alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/tmapctrl4.png" draggable="false" style="position: absolute; left: -141px; top: -13px; width: 170px; height: 54px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="gmnoprint" style="margin: 10px; z-index: 0; position: absolute; cursor: pointer; left: 0px; top: 0px;">
                                                        <div class="gm-style-mtc" style="float: left; position: relative;">
                                                            <div role="button" tabindex="0" title="Show street map" aria-label="Show street map" aria-pressed="true" style="direction: ltr; overflow: hidden; text-align: center; position: relative; color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 11px; border-bottom-left-radius: 2px; border-top-left-radius: 2px; -webkit-background-clip: padding-box; background-clip: padding-box; box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px; min-width: 22px; font-weight: 500;" draggable="false">Map</div>
                                                            <div style="background-color: white; z-index: -1; padding: 2px; border-bottom-left-radius: 2px; border-bottom-right-radius: 2px; box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px; position: absolute; left: 0px; top: 35px; text-align: left; display: none;">
                                                                <div style="color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 8px; direction: ltr; text-align: left; white-space: nowrap;" draggable="false" title="Show street map with terrain"><span role="checkbox" style="box-sizing: border-box; position: relative; line-height: 0; font-size: 0px; margin: 0px 5px 0px 0px; display: inline-block; background-color: rgb(255, 255, 255); border: 1px solid rgb(198, 198, 198); border-radius: 1px; width: 13px; height: 13px; vertical-align: middle;"><div style="position: absolute; left: 1px; top: -2px; width: 13px; height: 11px; overflow: hidden; display: none;"><img alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/imgs8.png" draggable="false" style="position: absolute; left: -52px; top: -44px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 68px; height: 67px;"></div></span>
                                                                    <label style="vertical-align: middle; cursor: pointer;">Terrain</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="gm-style-mtc" style="float: left; position: relative;">
                                                            <div role="button" tabindex="0" title="Show satellite imagery" aria-label="Show satellite imagery" aria-pressed="false" style="direction: ltr; overflow: hidden; text-align: center; position: relative; color: rgb(86, 86, 86); font-family: Roboto, Arial, sans-serif; user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 11px; border-bottom-right-radius: 2px; border-top-right-radius: 2px; -webkit-background-clip: padding-box; background-clip: padding-box; box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px; min-width: 40px; border-left: 0px;" draggable="false">Satellite</div>
                                                            <div style="background-color: white; z-index: -1; padding: 2px; border-bottom-left-radius: 2px; border-bottom-right-radius: 2px; box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 4px -1px; position: absolute; right: 0px; top: 35px; text-align: left; display: none;">
                                                                <div style="color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; user-select: none; font-size: 11px; background-color: rgb(255, 255, 255); padding: 8px; direction: ltr; text-align: left; white-space: nowrap;" draggable="false" title="Show imagery with street names"><span role="checkbox" style="box-sizing: border-box; position: relative; line-height: 0; font-size: 0px; margin: 0px 5px 0px 0px; display: inline-block; background-color: rgb(255, 255, 255); border: 1px solid rgb(198, 198, 198); border-radius: 1px; width: 13px; height: 13px; vertical-align: middle;"><div style="position: absolute; left: 1px; top: -2px; width: 13px; height: 11px; overflow: hidden;"><img alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/imgs8.png" draggable="false" style="position: absolute; left: -52px; top: -44px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 68px; height: 67px;"></div></span>
                                                                    <label style="vertical-align: middle; cursor: pointer;">Labels</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div ng-transclude="" style="display: none">
                                            <span class="angular-google-map-marker ng-scope ng-isolate-scope" ng-transclude="" coords="houseMarker.coords" options="houseMarker.options" idkey="houseMarker.id">
          <span class="angular-google-maps-window ng-scope ng-isolate-scope" ng-transclude="" options="houseMarker.infoWindow.options" events="houseMarker.infoWindow.events">
            <div class="ng-scope">
              <strong class="ng-binding">Nathu Nest Delhi</strong>
              <p class="ng-binding">40, siddhartha enclave, new delhi, delhi, india, delhi- 110014</p>
            </div>
          </span>
                                            </span>
                                            <span class="angular-google-map-markers ng-scope ng-isolate-scope" ng-transclude="" models="loadedMarkers" coords="&#39;coords&#39;" icon="&#39;options.icon&#39;" options="&#39;options&#39;" idkey="id"></span>
                                            <!-- ngIf: polyline != null -->
                                        </div>
                                    </div>
                                </ui-gmap-google-map>
                            </div>
                        </div>
                    </div>
                    <!-- on the map -->
                </section>
                <!-- near by -->
            </div>
        </div> */ ?>
        <!-- similar listing -->
       <?php /* <div id="similar" class="nav-section">
            <div class="grid">
                <div class="similar-house-widget ng-scope" id="similar-houses-app" ng-controller="FetchController" ng-init="init(&#39;7860&#39;, &#39;Bed&#39;)">
                    <h2 ng-show="hasHouses" id="similar-placeholder" class=""> Similar Houses </h2>
                    <div class="owl-carousel owl-theme" id="similar-suggestion" style="opacity: 1; display: block;">
                        <!-- ngRepeat: house in houses -->
                        <div class="owl-wrapper-outer">
                            <div class="owl-wrapper" style="width: 6760px; left: 0px; display: block; transition: all 0ms ease; transform: translate3d(0px, 0px, 0px);">
                                <div class="owl-item" style="width: 338px;">
                                    <div class="item ng-scope" ng-repeat="house in houses" on-last-repeat="">
                                        <div itemscope="" itemtype="">
                                            <a ng-href="/houses/sharing-room-for-rent-only-boys-in-kalkaji-delhi-4972?sim_h=1" title="Singh Nest Kalkaji" itemprop="url" href="">
                                                <div class="similar-list-card" ng-click="openHouseDetails(house)">
                                                    <div class="over-color">
                                                        <div class="result-box">
                                                            <div class="search-slider">
                                                                <span class="gender-badge ng-binding">boys</span>
                                                                <div ng-show="house.offers" class="offer-tag ng-hide"><span>Offer</span></div>
                                                                <img alt="Singh Nest Kalkaji 4 BHK Independent House Society" ng-src="" src="<?php echo base_url(); ?>assets/front/detal-vendors/thumb_medium_3ff92890-7429-4c36-b303-5c1aebd0c28e.webp">
                                                                <div class="mob-price-div price-gender-div">
                                                                    <div class="rent-value ng-binding">
                                                                        <p>Rent starts from</p>
                                                                        ₹ 6000
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="similar-item-detail">
                                                            <div class="col-1-2">
                                                                <h3 class="ellipsis tittle ng-binding" itemprop="name">Singh Nest Kalkaji</h3>
                                                            </div>
                                                            <div class="col-1-2 text-right pad-r-zero ellipsis ng-binding">Kalkaji</div>
                                                            <div class="clr"></div>
                                                            <div>
                                                                <div class="col-1-2 ellipsis ng-binding">
                                                                    4 BHK - Independent House
                                                                </div>
                                                                <div class="col-1-2 text-right pad-r-zero ellipsis">
                                                                    <b class="ng-binding">2 beds available</b>
                                                                </div>
                                                                <div class="clr"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="owl-item" style="width: 338px;">
                                    <div class="item ng-scope" ng-repeat="house in houses" on-last-repeat="">
                                        <div itemscope="" itemtype="">
                                            <a ng-href="/houses/sharing-room-for-rent-only-boys-in-kalkaji-delhi-4972?sim_h=1" title="Singh Nest Kalkaji" itemprop="url" href="">
                                                <div class="similar-list-card" ng-click="openHouseDetails(house)">
                                                    <div class="over-color">
                                                        <div class="result-box">
                                                            <div class="search-slider">
                                                                <span class="gender-badge ng-binding">boys</span>
                                                                <div ng-show="house.offers" class="offer-tag ng-hide"><span>Offer</span></div>
                                                                <img alt="Singh Nest Kalkaji 4 BHK Independent House Society" ng-src="" src="<?php echo base_url(); ?>assets/front/detal-vendors/thumb_medium_3ff92890-7429-4c36-b303-5c1aebd0c28e.webp">
                                                                <div class="mob-price-div price-gender-div">
                                                                    <div class="rent-value ng-binding">
                                                                        <p>Rent starts from</p>
                                                                        ₹ 6000
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="similar-item-detail">
                                                            <div class="col-1-2">
                                                                <h3 class="ellipsis tittle ng-binding" itemprop="name">Singh Nest Kalkaji</h3>
                                                            </div>
                                                            <div class="col-1-2 text-right pad-r-zero ellipsis ng-binding">Kalkaji</div>
                                                            <div class="clr"></div>
                                                            <div>
                                                                <div class="col-1-2 ellipsis ng-binding">
                                                                    4 BHK - Independent House
                                                                </div>
                                                                <div class="col-1-2 text-right pad-r-zero ellipsis">
                                                                    <b class="ng-binding">2 beds available</b>
                                                                </div>
                                                                <div class="clr"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="owl-item" style="width: 338px;">
                                    <div class="item ng-scope" ng-repeat="house in houses" on-last-repeat="">
                                        <div itemscope="" itemtype="">
                                            <a ng-href="/houses/sharing-room-for-rent-only-boys-in-kalkaji-delhi-4972?sim_h=1" title="Singh Nest Kalkaji" itemprop="url" href="">
                                                <div class="similar-list-card" ng-click="openHouseDetails(house)">
                                                    <div class="over-color">
                                                        <div class="result-box">
                                                            <div class="search-slider">
                                                                <span class="gender-badge ng-binding">boys</span>
                                                                <div ng-show="house.offers" class="offer-tag ng-hide"><span>Offer</span></div>
                                                                <img alt="Singh Nest Kalkaji 4 BHK Independent House Society" ng-src="" src="<?php echo base_url(); ?>assets/front/detal-vendors/thumb_medium_3ff92890-7429-4c36-b303-5c1aebd0c28e.webp">
                                                                <div class="mob-price-div price-gender-div">
                                                                    <div class="rent-value ng-binding">
                                                                        <p>Rent starts from</p>
                                                                        ₹ 6000
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="similar-item-detail">
                                                            <div class="col-1-2">
                                                                <h3 class="ellipsis tittle ng-binding" itemprop="name">Singh Nest Kalkaji</h3>
                                                            </div>
                                                            <div class="col-1-2 text-right pad-r-zero ellipsis ng-binding">Kalkaji</div>
                                                            <div class="clr"></div>
                                                            <div>
                                                                <div class="col-1-2 ellipsis ng-binding">
                                                                    4 BHK - Independent House
                                                                </div>
                                                                <div class="col-1-2 text-right pad-r-zero ellipsis">
                                                                    <b class="ng-binding">2 beds available</b>
                                                                </div>
                                                                <div class="clr"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end ngRepeat: house in houses -->
                        <!-- end ngRepeat: house in houses -->
                        <!-- end ngRepeat: house in houses -->
                        <!-- end ngRepeat: house in houses -->
                        <!-- end ngRepeat: house in houses -->
                        <!-- end ngRepeat: house in houses -->
                        <!-- end ngRepeat: house in houses -->
                        <!-- end ngRepeat: house in houses -->
                        <!-- end ngRepeat: house in houses -->
                        <!-- end ngRepeat: house in houses -->
                        <div class="owl-controls">
                            <div class="owl-buttons">
                                <div class="owl-prev"><i class="icon-uniE6A0"></i></div>
                                <div class="owl-next"><i class="icon-uniE6AD"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> */ ?>
        <!-- similar listing -->
    </div>
    <!-- right side widget -->
    <!-- main details -->
    <?php /* <div class="mainContainer">
        <!--==== Adress part ====-->
        <div class="house-detail-container">
            <div id="session-new-popup" class="hide session-new-popup">
                <div class="movein-overlay" id="login-overlay"></div>
                
                <style type="text/css">
                .ui-autocomplete.ui-widget {
                    font-size: 10px;
                }
                </style>
                <!--==== middile ====-->
                <section>
                    <div class="login-wrapper main-login login-section  login-new-widget sign-partial">
                        <div class="col-4-12">
                            <div class="referral_login_sidebar">
                            </div>
                            <div class="">
                                <div class="login-left-info">
                                    <ul>
                                        <li>
                                            <span class="col-2-12">
        <i class="icon-uniE6E9"></i>
      </span>
                                            <span class="col-10-12">
        <h3>Search</h3>
        <p>
          Find a place that fits your all your preferences.
        </p>
      </span>
                                        </li>
                                        <li>
                                            <span class="col-2-12">
         <i class="icon-uniE67E"></i>
      </span>
                                            <span class="col-10-12">
        <h3>Visit</h3>
        <p>
          Finalise a time and date to visit the homes you like.
        </p>
      </span>
                                        </li>
                                        <li>
                                            <span class="col-2-12">
        <i class="icon-uniE6BD"></i>
      </span>
                                            <span class="col-10-12">
        <h3>Book</h3>
        <p>
          Pay a token amount online to lock the
          bed / room / house.
        </p>
      </span>
                                        </li>
                                        <li>
                                            <span class="col-2-12">
        <i class="icon-uniE6EB"></i>
      </span>
                                            <span class="col-10-12">
        <h3>Move-In</h3>
        <p>
          Choose a move-in date and just show up!
        </p>
      </span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-8-12 pad-0">
                            <span id="movein-cancel-btn" class="">
        <i class="icon-uniE6EF"></i>
      </span>
                            <div class="login-container">
                                <!-- <h1>Login</h1> -->
                                <!-- login via social -->
                                <div class="panel-body">
                                    <div class="collapse tab-pane fade in active" id="login-tab">
                                        <form accept-charset="UTF-8" action="" class="new_user" id="new_user" method="post">
                                            <div style="margin:0;padding:0;display:inline">
                                                <input name="utf8" type="hidden" value="✓">
                                                <input name="authenticity_token" type="hidden" value="N61theuN8gscD/QzKKeJYzdk5nfnYxBsKTDU1kZHpM8=">
                                            </div>
                                            <div id="login-template" class="input-box">
                                                <h2>Sign In</h2>
                                                <p class="text-left">Enter your email id or phone number</p>
                                                <input id="email-or-phone-value" name="email_or_phone_value" type="text" placeholder="Email id or phone number" autofocus="autofocus">
                                                <br>
                                                <br>
                                                <div class="user-action">
                                                    <input id="continue-btn" class="btn btn-primary btn-block btn-lg text-center btn-padding" value="Continue" type="button">
                                                </div>
                                            </div>
                                            <div id="login-by-email" class="input-box" style="display:none">
                                                <h2>Sign In</h2>
                                                <input id="email-login" name="user[email]" type="hidden">
                                                <p class="text-left">Enter the password for your account</p>
                                                <input id="password" name="user[password]" type="password" placeholder="Password" autocomplete="false">
                                                <span class="bar"></span>
                                                <div class="forgot-remem">
                                                    <div class="col-1-2 remem-me">
                                                        <input name="user[remember_me]" type="hidden" value="0">
                                                        <input class="w-auto" id="user_remember_me" name="user[remember_me]" type="checkbox" value="1">
                                                        <label for="user_remember_me">Remember me</label>
                                                    </div>
                                                    <div class="col-1-2 text-right">
                                                        <a href="">Forgot password?</a>
                                                    </div>
                                                    <div class="clr"></div>
                                                </div>
                                                <div class="">
                                                    <div class="col-1-2">
                                                        <input type="button" id="email-back-btn" class="btn btn-default btn-block btn-lg text-center btn-padding border-d" value="Back">
                                                    </div>
                                                    <div class="col-1-2">
                                                        <input type="submit" id="login-by-email-btn" class="btn btn-primary btn-block btn-lg text-center btn-padding" value="Login">
                                                    </div>
                                                    <div class="clr"></div>
                                                </div>
                                            </div>
                                            <div id="login-by-phone" style="display:none">
                                                <input id="phone-login" name="user[profile_attributes][phone]" type="hidden">
                                                <span id="otp-msg">Enter the OTP which was sent to your phone number</span>&nbsp;<span id="phone-num-txt" style="font-weight:bold;"></span>
                                                <input id="otp-secret-key" name="user[otp_secret_key]" type="text" placeholder="OTP" class="form-control input-md">
                                                <div class="login-buttons">
                                                    <div class="col-1-2">
                                                        <input type="button" id="phone-back-btn" class="login-btns back-btn submit" value="Back">
                                                    </div>
                                                    <div class="col-1-2">
                                                        <input type="submit" id="login-by-phone-btn" class="login-btns login-btn submit" value="Login">
                                                    </div>
                                                    <div class="col-1-1">
                                                        <span id="resend-otp-code" style="cursor:pointer;color:#f25362;text-decoration: underline;">Resend OTP</span>
                                                    </div>
                                                    <div class="clr"></div>
                                                </div>
                                            </div>
                                            <span id="error-msg" class="error-msg"></span>
                                            <div class="login-separator"><span>OR</span></div>
                                            <div class="login-social-widget">
                                                <span class="fb-login-btn col-1-2">
                    <a href="">
                      <img alt="Facebook-icn" src="<?php echo base_url(); ?>assets/front/detal-vendors/facebook-icn-db42becaa6b2bb9d04e94ed9787a1003.svg">
                      <span class="login-text">Login with Facebook</span>
                                                </a>
                                                </span>
                                                <span class="goo-login-btn flRt">
                    <a href="">
                      <img alt="Google-icn" src="<?php echo base_url(); ?>assets/front/detal-vendors/google-icn-fd90cb6089e814d219e258830442a2c7.svg">
                      <span class="login-text">Login with Google</span>
                                                </a>
                                                </span>
                                            </div>
                                        </form>
                                        <div>
                                            <p class="sign-up-nav-link">Don’t have an account? &nbsp;
                                                <span id="signup" class="signin-link">Sign up</span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="collapse tab-pane fade active" id="signup-tab">
                                        <h2>Sign Up</h2>
                                        <!-- for affiliate user -->
                                        <!-- for affiliate user -->
                                        <div id="for-affiliate" class="">
                                            <div class="login-social-widget">
                                                <span class="fb-login-btn col-1-2">
                    <a href="">
                      <img alt="Facebook-icn" src="<?php echo base_url(); ?>assets/front/detal-vendors/facebook-icn-db42becaa6b2bb9d04e94ed9787a1003.svg">
                      <span class="login-text">Sign up with Facebook</span>
                                                </a>
                                                </span>
                                                <span class="goo-login-btn flRt">
                    <a href="">
                      <img alt="Google-icn" src="<?php echo base_url(); ?>assets/front/detal-vendors/google-icn-fd90cb6089e814d219e258830442a2c7.svg">
                      <span class="login-text">Sign up with Google</span>
                                                </a>
                                                </span>
                                                <div class="clr"></div>
                                            </div>
                                            <div class="login-separator"><span>OR</span></div>
                                            <form accept-charset="UTF-8" action="" class="new_user" id="signup-form" method="post">
                                                <div style="margin:0;padding:0;display:inline">
                                                    <input name="utf8" type="hidden" value="✓">
                                                    <input name="authenticity_token" type="hidden" value="N61theuN8gscD/QzKKeJYzdk5nfnYxBsKTDU1kZHpM8=">
                                                </div>
                                                <input type="hidden" value="" name="user[profile_attributes[document_proof_attributes[identity_id]]]">
                                                <input type="hidden" value="" name="user_prefs[signup_mode]" id="mode">
                                                <div class="input-box">
                                                    <div class="col-1-2" style="padding-right: 10px;">
                                                        <input autofocus="autofocus" class="" id="first-name" name="user[profile_attributes[first_name]]" placeholder="First Name" type="text" value="">
                                                        <span class="bar"></span>
                                                        <div id="first-name-error" class="error-msg"></div>
                                                    </div>
                                                    <div class="col-1-2">
                                                        <input class="" id="last-name" name="user[profile_attributes[last_name]]" placeholder="Last Name" type="text" value="">
                                                        <span class="bar"></span>
                                                        <div id="last-name-error" class="error-msg"></div>
                                                    </div>
                                                </div>
                                                <div class="clr"></div>
                                                <div class="input-box">
                                                    <input autofocus="autofocus" class="" id="signup-email" name="user[email]" placeholder="Enter Email" size="30" type="email">
                                                    <span class="bar"></span>
                                                    <div id="signup-email-error" class="error-msg"></div>
                                                </div>
                                                <div class="input-box">
                                                    <div class="col-4-12">
                                                        <select class="custom" id="signup-country-code" name="user[profile_attributes[country_code]]" required="required">
                                                            <option value="1">+1</option>
                                                            <option value="7">+7</option>
                                                            <option value="20">+20</option>
                                                            <option value="27">+27</option>
                                                            <option value="30">+30</option>
                                                            <option value="31">+31</option>
                                                            <option value="32">+32</option>
                                                            <option value="33">+33</option>
                                                            <option value="34">+34</option>
                                                            <option value="36">+36</option>
                                                            <option value="39">+39</option>
                                                            <option value="40">+40</option>
                                                            <option value="41">+41</option>
                                                            <option value="43">+43</option>
                                                            <option value="44">+44</option>
                                                            <option value="45">+45</option>
                                                            <option value="46">+46</option>
                                                            <option value="47">+47</option>
                                                            <option value="48">+48</option>
                                                            <option value="49">+49</option>
                                                            <option value="51">+51</option>
                                                            <option value="52">+52</option>
                                                            <option value="53">+53</option>
                                                            <option value="54">+54</option>
                                                            <option value="55">+55</option>
                                                            <option value="56">+56</option>
                                                            <option value="57">+57</option>
                                                            <option value="58">+58</option>
                                                            <option value="60">+60</option>
                                                            <option value="61">+61</option>
                                                            <option value="62">+62</option>
                                                            <option value="63">+63</option>
                                                            <option value="64">+64</option>
                                                            <option value="65">+65</option>
                                                            <option value="66">+66</option>
                                                            <option value="81">+81</option>
                                                            <option value="82">+82</option>
                                                            <option value="84">+84</option>
                                                            <option value="86">+86</option>
                                                            <option value="90">+90</option>
                                                            <option value="91" selected="selected">+91</option>
                                                            <option value="92">+92</option>
                                                            <option value="93">+93</option>
                                                            <option value="94">+94</option>
                                                            <option value="95">+95</option>
                                                            <option value="98">+98</option>
                                                            <option value="211">+211</option>
                                                            <option value="212">+212</option>
                                                            <option value="213">+213</option>
                                                            <option value="216">+216</option>
                                                            <option value="218">+218</option>
                                                            <option value="220">+220</option>
                                                            <option value="221">+221</option>
                                                            <option value="222">+222</option>
                                                            <option value="223">+223</option>
                                                            <option value="224">+224</option>
                                                            <option value="225">+225</option>
                                                            <option value="226">+226</option>
                                                            <option value="227">+227</option>
                                                            <option value="228">+228</option>
                                                            <option value="229">+229</option>
                                                            <option value="230">+230</option>
                                                            <option value="231">+231</option>
                                                            <option value="232">+232</option>
                                                            <option value="233">+233</option>
                                                            <option value="234">+234</option>
                                                            <option value="235">+235</option>
                                                            <option value="236">+236</option>
                                                            <option value="237">+237</option>
                                                            <option value="238">+238</option>
                                                            <option value="239">+239</option>
                                                            <option value="240">+240</option>
                                                            <option value="241">+241</option>
                                                            <option value="242">+242</option>
                                                            <option value="243">+243</option>
                                                            <option value="244">+244</option>
                                                            <option value="245">+245</option>
                                                            <option value="246">+246</option>
                                                            <option value="248">+248</option>
                                                            <option value="249">+249</option>
                                                            <option value="250">+250</option>
                                                            <option value="251">+251</option>
                                                            <option value="252">+252</option>
                                                            <option value="253">+253</option>
                                                            <option value="254">+254</option>
                                                            <option value="255">+255</option>
                                                            <option value="256">+256</option>
                                                            <option value="257">+257</option>
                                                            <option value="258">+258</option>
                                                            <option value="260">+260</option>
                                                            <option value="261">+261</option>
                                                            <option value="262">+262</option>
                                                            <option value="263">+263</option>
                                                            <option value="264">+264</option>
                                                            <option value="265">+265</option>
                                                            <option value="266">+266</option>
                                                            <option value="267">+267</option>
                                                            <option value="268">+268</option>
                                                            <option value="269">+269</option>
                                                            <option value="290">+290</option>
                                                            <option value="291">+291</option>
                                                            <option value="297">+297</option>
                                                            <option value="298">+298</option>
                                                            <option value="299">+299</option>
                                                            <option value="350">+350</option>
                                                            <option value="351">+351</option>
                                                            <option value="352">+352</option>
                                                            <option value="353">+353</option>
                                                            <option value="354">+354</option>
                                                            <option value="355">+355</option>
                                                            <option value="356">+356</option>
                                                            <option value="357">+357</option>
                                                            <option value="358">+358</option>
                                                            <option value="359">+359</option>
                                                            <option value="370">+370</option>
                                                            <option value="371">+371</option>
                                                            <option value="372">+372</option>
                                                            <option value="373">+373</option>
                                                            <option value="374">+374</option>
                                                            <option value="375">+375</option>
                                                            <option value="376">+376</option>
                                                            <option value="377">+377</option>
                                                            <option value="378">+378</option>
                                                            <option value="380">+380</option>
                                                            <option value="381">+381</option>
                                                            <option value="382">+382</option>
                                                            <option value="385">+385</option>
                                                            <option value="386">+386</option>
                                                            <option value="387">+387</option>
                                                            <option value="389">+389</option>
                                                            <option value="420">+420</option>
                                                            <option value="421">+421</option>
                                                            <option value="423">+423</option>
                                                            <option value="500">+500</option>
                                                            <option value="501">+501</option>
                                                            <option value="502">+502</option>
                                                            <option value="503">+503</option>
                                                            <option value="504">+504</option>
                                                            <option value="505">+505</option>
                                                            <option value="506">+506</option>
                                                            <option value="507">+507</option>
                                                            <option value="508">+508</option>
                                                            <option value="509">+509</option>
                                                            <option value="590">+590</option>
                                                            <option value="591">+591</option>
                                                            <option value="592">+592</option>
                                                            <option value="593">+593</option>
                                                            <option value="594">+594</option>
                                                            <option value="595">+595</option>
                                                            <option value="596">+596</option>
                                                            <option value="597">+597</option>
                                                            <option value="598">+598</option>
                                                            <option value="599">+599</option>
                                                            <option value="670">+670</option>
                                                            <option value="672">+672</option>
                                                            <option value="673">+673</option>
                                                            <option value="674">+674</option>
                                                            <option value="675">+675</option>
                                                            <option value="676">+676</option>
                                                            <option value="677">+677</option>
                                                            <option value="678">+678</option>
                                                            <option value="679">+679</option>
                                                            <option value="680">+680</option>
                                                            <option value="681">+681</option>
                                                            <option value="682">+682</option>
                                                            <option value="683">+683</option>
                                                            <option value="685">+685</option>
                                                            <option value="686">+686</option>
                                                            <option value="687">+687</option>
                                                            <option value="688">+688</option>
                                                            <option value="689">+689</option>
                                                            <option value="690">+690</option>
                                                            <option value="691">+691</option>
                                                            <option value="692">+692</option>
                                                            <option value="850">+850</option>
                                                            <option value="852">+852</option>
                                                            <option value="853">+853</option>
                                                            <option value="855">+855</option>
                                                            <option value="856">+856</option>
                                                            <option value="880">+880</option>
                                                            <option value="886">+886</option>
                                                            <option value="960">+960</option>
                                                            <option value="961">+961</option>
                                                            <option value="962">+962</option>
                                                            <option value="963">+963</option>
                                                            <option value="964">+964</option>
                                                            <option value="965">+965</option>
                                                            <option value="966">+966</option>
                                                            <option value="967">+967</option>
                                                            <option value="968">+968</option>
                                                            <option value="970">+970</option>
                                                            <option value="971">+971</option>
                                                            <option value="972">+972</option>
                                                            <option value="973">+973</option>
                                                            <option value="974">+974</option>
                                                            <option value="975">+975</option>
                                                            <option value="976">+976</option>
                                                            <option value="977">+977</option>
                                                            <option value="992">+992</option>
                                                            <option value="993">+993</option>
                                                            <option value="994">+994</option>
                                                            <option value="995">+995</option>
                                                            <option value="996">+996</option>
                                                            <option value="998">+998</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-8-12 col-8-phone">
                                                        <input class="" id="signup-phone" name="user[profile_attributes[phone]]" placeholder="Enter Phone" type="text" value="">
                                                        <span class="bar"></span>
                                                        <div id="signup-phone-error" class="error-msg"></div>
                                                    </div>
                                                    <div class="clr"></div>
                                                </div>
                                                <div class="input-box col-1-2" style="padding-right: 10px;">
                                                    <input autocomplete="off" class="" id="signup-password" name="user[password]" placeholder="Enter Password" required="required" size="30" type="password">
                                                    <span class="bar"></span>
                                                    <div id="signup-password-error" class="error-msg"></div>
                                                </div>
                                                <div class="input-box col-1-2">
                                                    <input autocomplete="off" class="" id="password-confirmation" name="user[password_confirmation]" placeholder="Re-enter Password" required="required" size="30" type="password">
                                                    <span class="bar"></span>
                                                    <div id="password-confirmation-error" class="error-msg"></div>
                                                </div>
                                                <div class="clr"></div>
                                                <input type="hidden" value="" name="user[signup_source]">
                                                <input type="hidden" value="" name="user[mode]">
                                                <div class="user-action">
                                                    <input type="button" id="register-btn-partial" class="btn btn-primary btn-block btn-lg" value="Sign Up">
                                                    <input class="login-btns login-btn" name="commit" style="display:none;" type="submit" value="Sign Up">
                                                </div>
                                                <p class="login-hyperlink terms">
                                                    By signing up, I agree to Nestaway’s <a href="" target="_blank">Terms of Use</a> and <a href="" target="_blank">Privacy Policy</a>.
                                                </p>
                                                <div class="login-hyperlink">
                                                    <span style="display:block; text-align:center;"><a href="">Didn't receive confirmation instructions?</a></span>
                                                    <br>
                                                </div>
                                            </form>
                                            <div>
                                                <p class="sign-up-nav-link">Have an account with us? &nbsp;<span class="signin-link" id="signin">Sign In</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="fb-root"></div>
                    </div>
                </section>
                
            </div>
            <div id="profile-complete-popup" class="hide session-new-popup">
                <div class="movein-overlay" id="profile-overlay"></div>
                <div class="login-wrapper">
                    <div class="login-container">
                        <div id="form_error_div" class="error" style="display:none;"></div>
                        <form name="profile-complete" action="" method="POST">
                            <div class="field-box">
                                <input type="text" id="first-name" name="first_name" value="" placeholder="First Name" required="required">
                            </div>
                            <div class="field-box">
                                <input type="text" id="last-name" name="last_name" value="" placeholder="Last Name" required="required">
                            </div>
                            <div class="field-box">
                                <input type="text" id="profile-phone" name="phone" value="" placeholder="Mobile Number" required="required">
                                <div class="loader-widget">
                                    <img alt="Loader" class="hide loader" src="<?php echo base_url(); ?>assets/front/detal-vendors/loader-9f9ceceeccb638be9eafaed947dbe1ac.gif">
                                    <i class="fa fa-check hide checked-phone"></i>
                                    <i class="fa fa-times hide phone-invalid"></i>
                                </div>
                            </div>
                            <p style="text-align:center">
                                <input class="login-btn" id="complete-profile" name="commit" type="submit" value="Complete your profile">
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> */ ?>
    
    <!--==== footer ====-->
    <div class="black-popup-overlay z-index-high"></div>
    <!--==== photo popup section ====-->
    <div class="banner-popup banner-popup-container" style="height: 622px;">
        <span class="close-popup2">×</span>
        <div id="home-gallery-items" class="owl-carousel owl-theme" style="opacity: 0; display: block;">
            <div class="owl-wrapper-outer">
                <div class="owl-wrapper" style="width: 1800px; left: 0px; display: block; transition: all 0ms ease; transform: translate3d(0px, 0px, 0px);">
                    <div class="owl-item" style="width: 100px;">
                        <div class="item slider-item" style="height: 622px;">
                            <div class="col-9-12 slider-item" style="height: 622px;">
                                <img src="<?php echo base_url(); ?>assets/front/detal-vendors/scaled_full_73b0adc1-ae36-410d-984c-57ce91f10747.webp" alt="Nathu Nest Delhi 3 BHK Flat Image for House N7860">
                            </div>
                            <div class="col-3-12 gallery-room-details">
                                <div class="house-modal-detail">
                                    <h2>Living Room</h2>
                                    <ul class="home-aminities">
                                        <li><i class="icon-uniE6CD"></i> Sofa</li>
                                        <li><i class="icon-uniE685"></i> Washing Machine</li>
                                        <li><i class="icon-uniE6E1"></i> WIFI</li>
                                        <li><i class="icon-uniE689"></i> Dish TV</li>
                                        <li><i class="icon-uniE6F3"></i> Air Conditioner</li>
                                    </ul>
                                </div>
                                <!-- footer price details -->
                                <div class="house-modal-detail-footer">
                                    <span class="modal-beds-available">2 beds
                          available</span>
                                </div>
                                <!-- footer price details -->
                            </div>
                        </div>
                    </div>
                    <div class="owl-item" style="width: 100px;">
                        <div class="item slider-item" style="height: 622px;">
                            <div class="col-9-12 slider-item" style="height: 622px;">
                                <img src="<?php echo base_url(); ?>assets/front/detal-vendors/scaled_full_c2f64002-b007-41ec-9063-1cb83fc5328f.webp" alt="Nathu Nest Delhi 3 BHK Flat Image for House N7860">
                            </div>
                            <div class="col-3-12 gallery-room-details">
                                <div class="house-modal-detail">
                                    <h2>dining</h2>
                                </div>
                                <!-- footer price details -->
                                <div class="house-modal-detail-footer">
                                    <span class="modal-beds-available">2 beds
                          available</span>
                                </div>
                                <!-- footer price details -->
                            </div>
                        </div>
                    </div>
                    <div class="owl-item" style="width: 100px;">
                        <div class="item slider-item" style="height: 622px;">
                            <div class="col-9-12 slider-item" style="height: 622px;">
                                <img src="<?php echo base_url(); ?>assets/front/detal-vendors/scaled_full_f712d4d9-1281-4981-94fa-20ad7bf8229b.webp" alt="Nathu Nest Delhi 3 BHK Flat Image for House N7860">
                            </div>
                            <div class="col-3-12 gallery-room-details">
                                <div class="house-modal-detail">
                                    <h2>Kitchen</h2>
                                    <ul class="home-aminities">
                                        <li><i class="icon-uniE656"></i> Dining Table</li>
                                        <li><i class="icon-uniE66B"></i> Fridge</li>
                                        <li><i class="icon-uniE62D"></i> Gas Stove</li>
                                    </ul>
                                </div>
                                <!-- footer price details -->
                                <div class="house-modal-detail-footer">
                                    <span class="modal-beds-available">2 beds
                          available</span>
                                </div>
                                <!-- footer price details -->
                            </div>
                        </div>
                    </div>
                    <div class="owl-item" style="width: 100px;">
                        <div class="item slider-item" style="height: 622px;">
                            <div class="col-9-12 slider-item" style="height: 622px;">
                                <img src="<?php echo base_url(); ?>assets/front/detal-vendors/scaled_full_df95aa4d-c3ea-4dd8-a8c7-555ad3a1fbe3.webp" alt="Nathu Nest Delhi 3 BHK Flat Image for room Room Number 1">
                            </div>
                            <div class="col-3-12 gallery-room-details">
                                <div class="house-modal-detail">
                                    <h2>Bedroom 1</h2>
                                    <ul class=" home-aminities">
                                        <li><i class="icon-uniE64A"></i> Cupboard</li>
                                    </ul>
                                </div>
                                <!-- footer price details -->
                                <div class="house-modal-detail-footer">
                                    <span class="modal-beds-available">2 beds
                        available</span>
                                </div>
                                <!-- footer price details -->
                            </div>
                        </div>
                    </div>
                    <div class="owl-item" style="width: 100px;">
                        <div class="item slider-item" style="height: 622px;">
                            <div class="col-9-12 slider-item" style="height: 622px;">
                                <img src="<?php echo base_url(); ?>assets/front/detal-vendors/scaled_full_a8509e71-96b2-401e-afff-c428080613c2.webp" alt="Nathu Nest Delhi 3 BHK Flat Image for room Room Number 1">
                            </div>
                            <div class="col-3-12 gallery-room-details">
                                <div class="house-modal-detail">
                                    <h2>Bathroom 1</h2>
                                    <ul class="home-aminities">
                                        <li><i class="icon-attached_bathroom"></i> Attached Bathroom</li>
                                        <li><i class="icon-uniE629"></i> Western Toilet</li>
                                    </ul>
                                </div>
                                <!-- footer price details -->
                                <div class="house-modal-detail-footer">
                                    <span class="modal-beds-available">2 beds
                        available</span>
                                </div>
                                <!-- footer price details -->
                            </div>
                        </div>
                    </div>
                    <div class="owl-item" style="width: 100px;">
                        <div class="item slider-item" style="height: 622px;">
                            <div class="col-9-12 slider-item" style="height: 622px;">
                                <img src="<?php echo base_url(); ?>assets/front/detal-vendors/scaled_full_25ddef49-f10d-4c9b-ae53-52370cb10f6f.webp" alt="Nathu Nest Delhi 3 BHK Flat Image for room Room Number 2">
                            </div>
                            <div class="col-3-12 gallery-room-details">
                                <div class="house-modal-detail">
                                    <h2>Bedroom 2</h2>
                                    <ul class=" home-aminities">
                                        <li><i class="icon-uniE64A"></i> Cupboard</li>
                                    </ul>
                                </div>
                                <!-- footer price details -->
                                <div class="house-modal-detail-footer">
                                    <span class="modal-beds-available">2 beds
                        available</span>
                                </div>
                                <!-- footer price details -->
                            </div>
                        </div>
                    </div>
                    <div class="owl-item" style="width: 100px;">
                        <div class="item slider-item" style="height: 622px;">
                            <div class="col-9-12 slider-item" style="height: 622px;">
                                <img src="<?php echo base_url(); ?>assets/front/detal-vendors/scaled_full_0e8b50c0-65f9-452d-a0e2-28e4ba0b30a5.webp" alt="Nathu Nest Delhi 3 BHK Flat Image for room Room Number 2">
                            </div>
                            <div class="col-3-12 gallery-room-details">
                                <div class="house-modal-detail">
                                    <h2>Bathroom 2</h2>
                                    <ul class="home-aminities">
                                        <li><i class="icon-attached_bathroom"></i> Attached Bathroom</li>
                                        <li><i class="icon-uniE629"></i> Western Toilet</li>
                                    </ul>
                                </div>
                                <!-- footer price details -->
                                <div class="house-modal-detail-footer">
                                    <span class="modal-beds-available">2 beds
                        available</span>
                                </div>
                                <!-- footer price details -->
                            </div>
                        </div>
                    </div>
                    <div class="owl-item" style="width: 100px;">
                        <div class="item slider-item" style="height: 622px;">
                            <div class="col-9-12 slider-item" style="height: 622px;">
                                <img src="<?php echo base_url(); ?>assets/front/detal-vendors/scaled_full_bb1b3d13-7f71-43e8-a9d7-0ba2d11c62f9.webp" alt="Nathu Nest Delhi 3 BHK Flat Image for room Room Number 3">
                            </div>
                            <div class="col-3-12 gallery-room-details">
                                <div class="house-modal-detail">
                                    <h2>Bedroom 3</h2>
                                    <ul class=" home-aminities">
                                        <li><i class="icon-uniE64A"></i> Cupboard</li>
                                    </ul>
                                </div>
                                <!-- footer price details -->
                                <div class="house-modal-detail-footer">
                                    <span class="modal-beds-available">2 beds
                        available</span>
                                </div>
                                <!-- footer price details -->
                            </div>
                        </div>
                    </div>
                    <div class="owl-item" style="width: 100px;">
                        <div class="item slider-item" style="height: 622px;">
                            <div class="col-9-12 slider-item" style="height: 622px;">
                                <img src="<?php echo base_url(); ?>assets/front/detal-vendors/scaled_full_60976622-857f-4af9-b9d0-df3447cc7801.webp" alt="Nathu Nest Delhi 3 BHK Flat Image for room Room Number 3">
                            </div>
                            <div class="col-3-12 gallery-room-details">
                                <div class="house-modal-detail">
                                    <h2>Bathroom 3</h2>
                                    <ul class="home-aminities">
                                        <li><i class="icon-attached_bathroom"></i> Attached Bathroom</li>
                                        <li><i class="icon-uniE629"></i> Western Toilet</li>
                                    </ul>
                                </div>
                                <!-- footer price details -->
                                <div class="house-modal-detail-footer">
                                    <span class="modal-beds-available">2 beds
                        available</span>
                                </div>
                                <!-- footer price details -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="owl-controls">
                <div class="owl-buttons">
                    <div class="owl-prev"><i class="icon-uniE6A0"></i></div>
                    <div class="owl-next"><i class="icon-uniE6AD"></i></div>
                </div>
            </div>
        </div>
    </div>
    <!--==== photo popup section ====-->
    <div class="hide-mobile house-detail-hack">
        <style text="stylesheet">
        .cancellation-policy {
            margin-top: 10px;
            color: #f25362;
            font-size: 12px;
            text-align: right;
            text-decoration: underline;
            cursor: pointer;
        }

        .cancellation-policy-dialog {
            max-width: 600px;
        }

        .cancellation-policy-dialog .modal-content {
            font-size: 14px;
            border-radius: 0;
            border: none;
            color: $gray-color;
        }

        .cancellation-policy-dialog .modal-content .modal-body {
            height: auto;
        }

        .cancellation-policy-dialog .modal-content .close-modal {
            position: absolute;
            right: 24px;
            top: 20px;
            z-index: 1;
            font-size: 30px;
            cursor: pointer;
        }

        .modal-backdrop {
            background: #333333;
        }

        .modal-backdrop.in {
            opacity: .9;
        }

        @media screen and (max-width: 767px) {
            .hide-mobile {
                display: none;
            }
        }
        </style>
        <div class="cancellation-policy" data-toggle="modal" data-target="#cancellation-policy">Cancellation Policy *</div>
        <div id="cancellation-policy" class="modal fade" role="dialog">
            <div class="modal-dialog cancellation-policy-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <span class="close-modal" data-dismiss="modal">×</span>
                        <h2>Cancellation Policy</h2>
                        <br>
                        <table class="table table-bordered">
                            <thead bgcolor="#f3f3f3">
                                <tr>
                                    <th class="col-1-2 align-left">Time of Cancellation</th>
                                    <th class="col-1-2">Cancellation Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Upto 2 days after booking</td>
                                    <td>₹ 0</td>
                                </tr>
                                <tr>
                                    <td>From 2 days after booking until Move-in date</td>
                                    <td><span class="js-cancellation-policy-token-amount">Token amount for the booking</span></td>
                                </tr>
                                <tr>
                                    <td>From Move-in date till 3 days after Move-in</td>
                                    <td><span class="js-cancellation-policy-onboarding-amount">On-boarding Charges</span> + Standard move out charges as per tenancy policy <a href="" target="_blank">Terms &amp; conditions.</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end here -->
    
    <link rel="publisher" href="">
    
    
    <link href="<?php echo base_url(); ?>assets/front/detal-vendors/login-new-b96f13d1503b4dacab8c3f4dc5118f7e.css" media="all" rel="stylesheet" type="text/css">
    <!-- JIRA Issue Collector -->
    <div id="cboxOverlay" style="display: none;"></div>
    <div id="colorbox" class="" role="dialog" tabindex="-1" style="display: none;">
        <div id="cboxWrapper">
            <div>
                <div id="cboxTopLeft" style="float: left;"></div>
                <div id="cboxTopCenter" style="float: left;"></div>
                <div id="cboxTopRight" style="float: left;"></div>
            </div>
            <div style="clear: left;">
                <div id="cboxMiddleLeft" style="float: left;"></div>
                <div id="cboxContent" style="float: left;">
                    <div id="cboxTitle" style="float: left;"></div>
                    <div id="cboxCurrent" style="float: left;"></div>
                    <button type="button" id="cboxPrevious"></button>
                    <button type="button" id="cboxNext"></button>
                    <button id="cboxSlideshow"></button>
                    <div id="cboxLoadingOverlay" style="float: left;"></div>
                    <div id="cboxLoadingGraphic" style="float: left;"></div>
                </div>
                <div id="cboxMiddleRight" style="float: left;"></div>
            </div>
            <div style="clear: left;">
                <div id="cboxBottomLeft" style="float: left;"></div>
                <div id="cboxBottomCenter" style="float: left;"></div>
                <div id="cboxBottomRight" style="float: left;"></div>
            </div>
        </div>
        <div style="position: absolute; width: 9999px; visibility: hidden; display: none; max-width: none;"></div>
    </div>
    
    
    
    <div style="width:0px; height:0px; display:none; visibility:hidden;" id="batBeacon0.35855661959492235"><img style="width:0px; height:0px; display:none; visibility:hidden;" id="batBeacon0.3584329667423529" width="0" height="0" alt="" src="<?php echo base_url(); ?>assets/front/detal-vendors/0"></div><span id="zg_4251555856564A455A515B59444C5E585C585F59" style="display: block !important;">                                                                                                    
    <style type="text/css">
    .zarget-poll-window {
        all: initial;
        background: #fff !important;
        width: 300px !important;
        -webkit-box-shadow: 0 0 7px 0 rgba(0, 0, 0, 0.6) !important;
        -moz-box-shadow: 0 0 7px 0 rgba(0, 0, 0, 0.6) !important;
        box-shadow: 0 0 7px 0 rgba(0, 0, 0, 0.6) !important;
        border: 1px solid rgba(0, 0, 0, 0.3) !important;
        display: block !important;
        padding: 2px;
        position: fixed;
        bottom: 0px;
        z-index: 9999999
    }

    .zarget-poll-window div,
    .zarget-poll-window span,
    .zarget-poll-window img,
    .zarget-poll-window ul,
    .zarget-poll-window li,
    .zarget-poll-window h1,
    .zarget-poll-window h2,
    .zarget-poll-window h3,
    .zarget-poll-window h4,
    .zarget-poll-window h5,
    .zarget-poll-window h6,
    .zarget-poll-window p,
    .zarget-poll-window a,
    .zarget-poll-window textarea,
    .zarget-poll-window label,
    .zarget-poll-window input[type="button"],
    .zarget-poll-window input[type="email"],
    .zarget-poll-window input[type="text"],
    .zarget-poll-window input[type="number"] {
        all: initial
    }

    .zarget-poll-window * {
        font-family: Arial, Helvetica, sans-serif !important;
        font-size: 12px !important;
        font-weight: normal !important;
        font-variant: normal !important;
        -webkit-font-smoothing: subpixel-antialiased !important;
        text-rendering: optimizeLegibility !important
    }

    .zarget-poll-window input[type="radio"],
    .zarget-poll-window input[type="checkbox"] {
        margin-right: 5px !important;
        width: 15px !important;
        height: 15px !important;
        background: #fff !important;
        cursor: pointer !important;
        display: inline-block !important;
        vertical-align: middle !important;
        border: 1px solid rgba(0, 0, 0, 0.3) !important;
        opacity: 1 !important
    }

    .zarget-poll-window input[type="radio"] {
        -webkit-border-radius: 50% !important;
        -moz-border-radius: 50% !important;
        border-radius: 50% !important;
        content: '' !important
    }

    .zarget-poll-window input[type="radio"]:checked {
        background: #fff !important;
        border: 4px solid #0190e2 !important
    }

    .zarget-poll-window input[type="checkbox"] {
        content: '' !important;
        -webkit-border-radius: 2px !important;
        -moz-border-radius: 2px !important;
        border-radius: 2px !important
    }

    .zarget-poll-window input[type="checkbox"]:checked {
        content: '' !important;
        background: #fff !important;
        border: 4px solid #0190e2 !important
    }

    .zarget-poll-window .active {
        display: block !important
    }

    .zarget-poll-window .inactive {
        display: none !important
    }

    .zarget-poll-window .zarget-poll-min-max {
        text-align: center !important;
        position: absolute !important;
        top: -19px !important;
        right: -1px !important;
        width: 40px !important;
        height: 16px !important;
        padding-top: 2px !important;
        cursor: pointer !important;
        -webkit-border-radius: 5px 5px 0 0 !important;
        -moz-border-radius: 5px 5px 0 0 !important;
        border-radiusus: 5px 5px 0 0 !important;
        background: #fff !important;
        border: 1px solid rgba(0, 0, 0, 0.3) !important;
        border-bottom: 0 solid !important;
        -webkit-box-shadow: 0px -3px 5px 0px rgba(0, 0, 0, 0.3) !important;
        -moz-box-shadow: 0px -3px 5px 0px rgba(0, 0, 0, 0.3) !important;
        box-shadow: 0px -3px 5px 0px rgba(0, 0, 0, 0.3) !important;
        text-align: center !important;
        -webkit-user-select: none !important;
        -moz-user-select: -moz-none !important;
        user-select: none !important;
        text-decoration: none !important
    }

    .zarget-poll-window .zarget-poll-min-max span {
        color: #000 !important;
        display: table !important;
        -moz-transform: rotate(-90deg) !important;
        -webkit-transform: rotate(-90deg) !important;
        transform: rotate(-90deg) !important;
        margin: 0 auto !important;
        cursor: pointer !important;
        -webkit-user-select: none !important;
        -moz-user-select: -moz-none !important;
        user-select: none !important
    }

    .zarget-poll-window .zarget-poll-title {
        display: block !important;
        padding: 20px 20px 20px 20px !important;
        font-weight: bold !important;
        line-height: 1.4em
    }

    .zarget-poll-window .zarget-poll-answers {
        display: block !important;
        padding: 0px 20px 20px 20px !important
    }

    .zarget-poll-window .zarget-poll-answers ul,
    .zarget-poll-window .zarget-poll-answers li {
        display: block !important
    }

    .zarget-poll-window .zarget-poll-answers ul {
        max-height: 190px !important;
        overflow-y: auto
    }

    .zarget-poll-window .zarget-poll-answers .zarget-single-select li,
    .zarget-poll-window .zarget-poll-answers .zarget-multi-select li {
        margin-bottom: 5px !important;
        background: rgba(255, 255, 255, 0.25) !important;
        -webkit-border-radius: 2px !important;
        -moz-border-radius: 2px !important;
        border-radius: 2px !important;
        padding: 3px 0px 3px 0px
    }

    .zarget-poll-window .zarget-poll-answers .zarget-single-select li label,
    .zarget-poll-window .zarget-poll-answers .zarget-multi-select li label {
        white-space: nowrap !important;
        overflow: hidden !important
    }

    .zarget-poll-window .zarget-poll-answers .zarget-single-select li label span::before,
    .zarget-poll-window .zarget-poll-answers .zarget-multi-select li label span::before {
        content: none
    }

    .zarget-poll-window .zarget-poll-answers .zarget-single-select input[type="radio"],
    .zarget-poll-window .zarget-poll-answers .zarget-single-select input[type="checkbox"],
    .zarget-poll-window .zarget-poll-answers .zarget-multi-select input[type="radio"],
    .zarget-poll-window .zarget-poll-answers .zarget-multi-select input[type="checkbox"] {
        margin: 0;
        margin-right: 5px !important
    }

    .zarget-poll-window .zarget-poll-answers .zarget-single-select span,
    .zarget-poll-window .zarget-poll-answers .zarget-multi-select span {
        cursor: pointer !important
    }

    .zarget-poll-window .zarget-poll-answers .zarget-single-select label,
    .zarget-poll-window .zarget-poll-answers .zarget-multi-select label {
        cursor: pointer !important;
        display: block !important;
        padding: 5px 3px 5px 10px !important
    }

    .zarget-poll-window .zarget-poll-answers .zarget-logtext-holder {
        display: table !important;
        width: 100% !important
    }

    .zarget-poll-window .zarget-poll-answers .zarget-logtext-holder .zarget-logtext {
        color: #000 !important;
        overflow: auto !important;
        resize: none !important;
        height: 120px !important;
        padding: 4px !important;
        width: 100% !important;
        background: #fff !important;
        -webkit-border-radius: 2px !important;
        -moz-border-radius: 2px !important;
        border-radius: 2px !important;
        border: 1px solid rgba(0, 0, 0, 0.2) !important;
        -webkit-box-sizing: border-box !important;
        -moz-box-sizing: border-box !important;
        box-sizing: border-box !important
    }

    .zarget-poll-window .zarget-poll-answers .zarget-shorttext-holder {
        display: table !important;
        width: 100% !important
    }

    .zarget-poll-window .zarget-poll-answers .zarget-shorttext-holder .zarget-shorttext {
        color: #000 !important;
        height: 30px !important;
        padding: 3px !important;
        width: 100% !important;
        background: #fff !important;
        margin-top: 25px !important;
        margin-bottom: 25px !important;
        -webkit-border-radius: 2px !important;
        -moz-border-radius: 2px !important;
        border-radius: 2px !important;
        border: 1px solid rgba(0, 0, 0, 0.2) !important;
        -webkit-box-sizing: border-box !important;
        -moz-box-sizing: border-box !important;
        box-sizing: border-box !important
    }

    .zarget-poll-window .zarget-poll-answers .zarget-description-holder {
        -webkit-box-sizing: border-box !important;
        -moz-box-sizing: border-box !important;
        box-sizing: border-box !important;
        padding-left: 10px !important;
        padding-right: 10px !important;
        display: none !important;
        position: relative
    }

    .zarget-poll-window .zarget-poll-answers .zarget-description-holder input.zarget-shorttext {
        margin-top: 10px !important;
        margin-bottom: 10px !important;
        -webkit-box-sizing: border-box !important;
        -moz-box-sizing: border-box !important;
        box-sizing: border-box !important
    }

    .zarget-poll-window .zarget-poll-answers .zarget-net-promoter {
        margin-top: 15px !important;
        margin-bottom: 30px !important;
        display: table !important;
        width: 100% !important
    }

    .zarget-poll-window .zarget-poll-answers .zarget-net-promoter .zarget-net-promoter-bg {
        background: #f2f2f2 !important;
        padding-top: 5px !important;
        padding-bottom: 5px !important;
        display: table !important;
        width: 100% !important
    }

    .zarget-poll-window .zarget-poll-answers .zarget-net-promoter ul li {
        border: 0px solid !important;
        display: inline-block !important;
        width: 20px !important;
        padding: 3px 0 3px 0 !important;
        margin: 0 !important;
        -webkit-border-radius: 2px !important;
        -moz-border-radius: 2px !important;
        border-radius: 2px !important;
        text-align: center !important;
        cursor: pointer
    }

    .zarget-poll-window .zarget-poll-answers .zarget-net-promoter ul li:hover {
        background: rgba(0, 0, 0, 0.2) !important
    }

    .zarget-poll-window .zarget-poll-answers .zarget-net-promoter .zarget-active,
    .zarget-poll-window .zarget-poll-answers .zarget-net-promoter .zarget-active:hover {
        background: #00cc66 !important;
        color: #fff !important
    }

    .zarget-poll-window .zarget-poll-answers .zarget-net-promoter .scrore-label-holder {
        margin-top: 10px !important;
        display: table !important;
        width: 100% !important
    }

    .zarget-poll-window .zarget-poll-answers .zarget-net-promoter .scrore-label-holder .zarget-fleft,
    .zarget-poll-window .zarget-poll-answers .zarget-net-promoter .scrore-label-holder .zarget-fright {
        width: 120px;
        overflow-wrap: break-word;
        word-wrap: break-word;
        word-break: break-all;
        word-break: break-word
    }

    .zarget-poll-window .zarget-poll-answers .zarget-net-promoter .zarget-fleft {
        float: left !important
    }

    .zarget-poll-window .zarget-poll-answers .zarget-net-promoter .zarget-fright {
        float: right !important;
        text-align: right !important
    }

    .zarget-poll-window .zarget-thank-mgs-holder {
        padding: 20px 20px 20px 20px !important
    }

    .zarget-poll-window .zarget-thank-mgs-holder .zarget-thank-mgs-bg {
        display: block !important;
        background: rgba(0, 0, 0, 0.05) !important;
        padding: 20px !important;
        text-align: center !important
    }

    .zarget-poll-window .zarget-thank-mgs-holder .zarget-thank-mgs {
        display: block !important;
        font-weight: bold !important;
        text-align: center !important;
        margin-top: 20px !important;
        margin-bottom: 20px !important;
        word-wrap: break-word;
        -ms-word-break: break-all;
        word-break: break-word
    }

    .zarget-poll-window .zarget-thank-mgs-holder .zarget-thank-mgs-icon {
        display: table !important;
        margin: 0 auto !important;
        color: #00ca7d !important;
        -webkit-box-sizing: border-box !important;
        -moz-box-sizing: border-box !important;
        box-sizing: border-box !important;
        margin-bottom: 20px !important;
        text-align: center !important;
        font-size: 40px !important;
        width: 50px !important;
        height: 50px !important;
        padding-top: 6px !important;
        -webkit-border-radius: 50% !important;
        -moz-border-radius: 50% !important;
        border-radius: 50% !important
    }

    .zarget-poll-window .zarget-thank-mgs-holder .zarget-thank-button {
        display: block !important;
        text-align: center !important
    }

    .zarget-poll-window .zarget-thank-mgs-holder .zarget-thank-button button {
        background: #4b4b4b !important;
        color: #fff !important;
        border: 1px solid #4b4b4b !important;
        height: 29px !important;
        padding-left: 20px !important;
        padding-right: 20px !important;
        -webkit-border-radius: 2px !important;
        -moz-border-radius: 2px !important;
        border-radius: 2px !important;
        cursor: pointer !important
    }

    .zarget-poll-window .zarget-thank-mgs-holder .zarget-thank-button button:focus {
        background: #4b4b4b !important;
        border: 1px solid #4b4b4b;
        color: #fff !important
    }

    .zarget-poll-window .zarget-thank-mgs-holder .zarget-thank-button button:hover {
        background: #262626 !important;
        border: 1px solid #262626 !important;
        color: #fff !important
    }

    .zarget-poll-window .zarget-poll-footer {
        display: block !important;
        padding: 5px 20px 20px 20px !important;
        overflow: auto !important
    }

    .zarget-poll-window .zarget-poll-footer .zarget-poll-powerdby {
        float: left !important;
        color: rgba(0, 0, 0, 0.54) !important;
        font-size: 10px !important;
        line-height: 28px !important
    }

    .zarget-poll-window .zarget-poll-footer .zarget-poll-powerdby a {
        color: rgba(0, 0, 0, 0.54) !important;
        font-size: 10px !important;
        cursor: pointer !important;
        text-decoration: underline !important
    }

    .zarget-poll-window .zarget-poll-footer .zarget-poll-powerdby a:hover {
        text-decoration: underline !important
    }

    .zarget-poll-window .zarget-poll-footer .zarget-poll-button {
        float: right !important
    }

    .zarget-poll-window .zarget-poll-footer .zarget-poll-button button {
        all: initial;
        background: #9f9f9f !important;
        border: 1px solid #9f9f9f !important;
        color: #000 !important;
        cursor: pointer !important;
        height: 28px;
        padding-right: 20px !important;
        padding-left: 20px !important;
        border-radius: 4px
    }

    .zarget-poll-window .zarget-poll-footer .zarget-poll-button button:hover {
        background: #4e4e4e !important;
        border: 1px solid #4e4e4e !important;
        color: #fff !important;
        -webkit-box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, 0.75) !important;
        -moz-box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, 0.75) !important;
        box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, 0.75) !important
    }

    .zarget-poll-window .zarget-poll-footer .zarget-poll-button button.disabled {
        background: rgba(0, 0, 0, 0.2) !important;
        cursor: not-allowed !important;
        color: #000 !important;
        border: 1px solid rgba(0, 0, 0, 0.2) !important;
        opacity: .38
    }

    .zarget-poll-window .zarget-poll-footer .zarget-poll-button button.disabled:focus,
    .zarget-poll-window .zarget-poll-footer .zarget-poll-button button.disabled:hover {
        background: 0, 0, 0, 0.2 !important;
        color: #000 !important;
        border: 1px solid rgba(0, 0, 0, 0.2) !important;
        opacity: .38;
        box-shadow: none !important
    }

    .zarget-poll-window.left {
        left: 20px
    }

    .zarget-poll-window.right {
        right: 100px
    }

    @media only screen and (max-device-width: 720px) {
        .zarget-poll-window {
            width: 100% !important;
            left: 0 !important;
            bottom: 0 !important
        }
    }

    @media only screen and (min-device-width: 320px) and (max-device-width: 568px) {
        .zarget-poll-window {
            width: 96% !important;
            left: 0 !important;
            right: 0 !important;
            bottom: 0 !important;
            margin-left: 2% !important;
            margin-right: 2% !important
        }
    }

    .zarget-poll-window.zarget-theme-skin1 {
        background: #fff9f6 !important;
        border: 1px solid #ff4d00 !important
    }

    .zarget-poll-window.zarget-theme-skin1 .zarget-poll-min-max {
        background: #fff9f6 !important;
        border: 1px solid #ff4d00 !important;
        border-bottom: 0 solid !important
    }

    .zarget-poll-window.zarget-theme-skin1 .zarget-poll-footer .zarget-poll-button button {
        background: #ff4d00 !important;
        color: #fff !important;
        border: 1px solid #ff4d00 !important
    }

    .zarget-poll-window.zarget-theme-skin1 .zarget-poll-footer .zarget-poll-button button:focus {
        background: #ff4d00 !important;
        border: 1px solid #ff4d00;
        color: #fff !important
    }

    .zarget-poll-window.zarget-theme-skin1 .zarget-poll-footer .zarget-poll-button button:hover {
        background: #cc3d00 !important;
        border: 1px solid #cc3d00 !important;
        color: #fff !important
    }

    .zarget-poll-window.zarget-poll-open {
        padding: 2px
    }

    .zarget-poll-window.zarget-poll-open .zarget-poll-container {
        display: block
    }

    .zarget-poll-window.zarget-poll-close {
        padding: 2px
    }

    .zarget-poll-window.zarget-poll-close .zarget-poll-container {
        display: none
    }

    .zarget-poll-window.zarget-poll-close .zarget-poll-min-max span {
        -moz-transform: rotate(90deg) !important;
        -webkit-transform: rotate(90deg) !important;
        transform: rotate(90deg) !important;
        margin-top: 2px !important
    }

    .zarget-poll-window.zarget-theme-skin3 {
        background: #2F2723 !important;
        border: 1px solid #2F2723 !important
    }

    .zarget-poll-window.zarget-theme-skin3 .zarget-poll-min-max {
        background: #2F2723 !important;
        border: 1px solid #2F2723 !important;
        border-bottom: 0 solid !important;
        color: #fff !important
    }

    .zarget-poll-window.zarget-theme-skin3 .zarget-poll-min-max span {
        color: #fff !important
    }

    .zarget-poll-window.zarget-theme-skin3 .zarget-poll-title,
    .zarget-poll-window.zarget-theme-skin3 .zarget-poll-answers .zarget-single-select span,
    .zarget-poll-window.zarget-theme-skin3 .zarget-poll-answers .zarget-multi-select span {
        color: #fff !important
    }

    .zarget-poll-window.zarget-theme-skin3 .zarget-thank-mgs-holder .zarget-thank-mgs {
        color: #fff
    }

    .zarget-poll-window.zarget-theme-skin3 .zarget-poll-answers .zarget-single-select li {
        background: rgba(255, 255, 255, 0.25) !important
    }

    .zarget-poll-window.zarget-theme-skin3 .scrore-label-holder .zarget-fleft,
    .zarget-poll-window.zarget-theme-skin3 .scrore-label-holder .zarget-fright {
        color: #fff !important
    }

    .zarget-poll-window.zarget-theme-skin3 .zarget-thank-mgs-holder .zarget-thank-mgs-bg {
        background: rgba(255, 255, 255, 0.2) !important
    }

    .zarget-poll-window.zarget-theme-skin3 .zarget-poll-footer .zarget-poll-powerdby {
        color: rgba(255, 255, 255, 0.54) !important
    }

    .zarget-poll-window.zarget-theme-skin3 .zarget-poll-footer .zarget-poll-powerdby a {
        color: rgba(255, 255, 255, 0.54) !important
    }

    .zarget-poll-window.zarget-theme-skin3 .zarget-poll-footer .zarget-poll-button button {
        background: #ff4d00 !important;
        color: #fff !important;
        border: 1px solid #ff4d00 !important
    }

    .zarget-poll-window.zarget-theme-skin3 .zarget-poll-footer .zarget-poll-button button:focus {
        background: #ff4d00 !important;
        border: 1px solid #ff4d00;
        color: #fff !important
    }

    .zarget-poll-window.zarget-theme-skin3 .zarget-poll-footer .zarget-poll-button button:hover {
        background: #cc3d00 !important;
        border: 1px solid #cc3d00 !important;
        color: #fff !important
    }

    .zarget-poll-window.zarget-theme-skin2 {
        background: #ff4d00 !important;
        border: 1px solid #ff4d00 !important
    }

    .zarget-poll-window.zarget-theme-skin2 .zarget-poll-min-max {
        background: #ff4d00 !important;
        border: 1px solid #ff4d00 !important;
        border-bottom: 0 solid !important;
        color: #fff !important
    }

    .zarget-poll-window.zarget-theme-skin2 .zarget-poll-title,
    .zarget-poll-window.zarget-theme-skin2 .zarget-poll-answers .zarget-single-select span {
        color: #fff !important
    }

    .zarget-poll-window.zarget-theme-skin2 .zarget-thank-mgs-holder .zarget-thank-mgs {
        color: #fff
    }

    .zarget-poll-window.zarget-theme-skin2 .zarget-poll-answers .zarget-single-select li {
        background: rgba(255, 255, 255, 0.25) !important
    }

    .zarget-poll-window.zarget-theme-skin2 .zarget-poll-powerdby {
        color: rgba(255, 255, 255, 0.54) !important
    }

    .zarget-poll-window.zarget-theme-skin2 .zarget-poll-powerdby a {
        color: rgba(255, 255, 255, 0.54) !important
    }

    .zarget-poll-window.zarget-theme-skin2 .zarget-poll-footer .zarget-poll-button button {
        background: #fff !important;
        color: ff4d00 !important;
        border: 1px solid #ff4d00 !important
    }

    .zarget-poll-window.zarget-theme-skin2 .zarget-poll-footer .zarget-poll-button button:focus {
        background: #ff4d00 !important;
        border: 1px solid #ff4d00;
        color: #fff !important
    }

    .zarget-poll-window.zarget-theme-skin2 .zarget-poll-footer .zarget-poll-button button:hover {
        background: #cc3d00 !important;
        border: 1px solid #cc3d00 !important;
        color: #fff !important
    }

    .zarget-poll-window .zarget-poll-answers .zarget-single-select li label span,
    .zarget-poll-window .zarget-poll-answers .zarget-multi-select li label span {
        width: 220px !important;
        overflow-wrap: break-word !important;
        word-wrap: break-word !important;
        -ms-word-break: break-all !important;
        word-break: break-all !important;
        -ms-hyphens: auto !important;
        -moz-hyphens: auto !important;
        -webkit-hyphens: auto !important;
        hyphens: auto !important;
        display: inline-block !important;
        line-height: 15px !important;
        vertical-align: top
    }
    </style>
    </span>
    <script>
         $( function() {
            $( "#datepickerdate" ).datepicker();
          } );
    </script>
