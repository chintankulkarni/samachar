     <link rel="stylesheet" href="<?php echo base_url('assets/front'); ?>/css/vertical.news.slider.css">
    <!-- masonry
   ================================================== -->
    <section id="content-wrap" class="site-page m-t-none">
        <div class="row">
            <div class="col-nine box p-none">
                <section>
                    <div class="text-center">
                        <div class="title-btn">
                            <!-- INTERNATIONAL -->
                        </div>
                    </div>
                    <h1 class="entry-title add-bottom m-b-20 text-center"><?php echo $current[0]->postTitle ?></h1>
                    <div class="col-twelve text-center about-info">
                        <div class="red"><?php echo $current[0]->postPrdate ?></div>
                      <!--   <div><i class="fa fa-envelope" aria-hidden="true"></i> | Updated: 23rd Nov 2017 01:30:10 PM</div> -->
                    </div>
                   <!--  <div class="content-media">
                        <img src="images/thumbs/about-us.jpg">
                    </div> -->
                    <div class="primary-content p-relative">
                        <div class="chare-btn"><a href="#"><i class="fa fa-share-alt" aria-hidden="true"></i></a></div>
                        <p><?php echo $current[0]->postCont ?></p>                       
                    </div>
                </section>
              <!--   <div class="row">
                    <nav class="pagination">
                        <span class="page-numbers prev"><i class="fa fa-angle-left" aria-hidden="true"></i></span>
                        <span class="page-numbers current">1</span>
                        <a href="#" class="page-numbers">2</a>
                        <a href="#" class="page-numbers">3</a>
                        <a href="#" class="page-numbers">4</a>
                        <a href="#" class="page-numbers">5</a>
                        <a href="#" class="page-numbers">6</a>
                        <a href="#" class="page-numbers">7</a>
                        <a href="#" class="page-numbers">8</a>
                        <a href="#" class="page-numbers">9</a>
                        <a href="#" class="page-numbers next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
                    </nav>
                </div>        -->         
            </div>
            <div class="col-three p-r-none">
                <article class="brick entry format-standard animate-this p-l-none">
                  <div class="entry-text">
                     <div class="entry-header">

                        <h1 class="entry-title"><a href="#">LATEST NEWS <i class="fa fa-angle-right pull-right" aria-hidden="true"></i></a></h1>
                        
                     </div>
                     <div class="entry-excerpt">
                        <ul>
                           <li><a href="">Odisha to present Rs 9,829 cr supplementary budget</a></li>
                           <li><a href="">Samsung to soon combine IoT Cloud with its products</a></li>
                           <li><a href="">Telangana to launch electric vehicle policy next month</a></li>
                           <li><a href="">IndiGo to add 8 extra flights from January 1</a></li>
                           <li><a href="">Congress seeks President’s intervention to convene</a></li>
                        </ul>
                     </div>
                  </div>

               </article> <!-- end article -->
               <article class="brick entry format-standard animate-this p-l-none m-t-40">
                  <div class="entry-text">
                     <div class="entry-header">

                        <h1 class="entry-title"><a href="#">LATEST NEWS <i class="fa fa-angle-right pull-right" aria-hidden="true"></i></a></h1>
                        
                     </div>
                     <div class="entry-excerpt">
                        <ul>
                           <li><a href="">Odisha to present Rs 9,829 cr supplementary budget</a></li>
                           <li><a href="">Samsung to soon combine IoT Cloud with its products</a></li>
                           <li><a href="">Telangana to launch electric vehicle policy next month</a></li>
                           <li><a href="">IndiGo to add 8 extra flights from January 1</a></li>
                           <li><a href="">Congress seeks President’s intervention to convene</a></li>
                        </ul>
                     </div>
                  </div>

               </article> <!-- end article -->
            </div>
            <!-- end col-twelve -->
        </div>
        <!-- end row -->
    </section>
    <section class="site-page m-t-none m-t-40 bottom-section m-b-40">
      <div class="row custom-row">
        <div class="col-nine p-l-none p-r-none bg-white slider-section">
          <div class="col-six p-none">
              <article class="brick entry format-standard animate-this p-l-none">
                  <div class="entry-text">
                      <div class="entry-header">
                          <h1 class="entry-title p-none"><a href="#">LATEST NEWS <i class="fa fa-angle-right pull-right" aria-hidden="true"></i></a></h1>
                      </div>
                      <div class="entry-excerpt">
                          <ul class="news-headlines">
                              <li class="selected"><a>Odisha to present Rs 9,829 cr supplementary budget</a></li>
                              <li><a>Samsung to soon combine IoT Cloud with its products</a></li>
                              <li><a>Telangana to launch electric vehicle policy next month</a></li>
                              <li><a>IndiGo to add 8 extra flights from January 1</a></li>
                              <li><a>Congress seeks President’s intervention to convene</a></li>
                              <li><a>IndiGo to add 8 extra flights from January 1</a></li>
                          </ul>
                      </div>
                  </div>
              </article>
              <!-- end article -->
          </div>
          <div class="news-preview col-six p-none">
            <div class="news-content top-content">
              <a href=""><img src="<?php echo base_url('assets/front'); ?>/images/thumbs/news-1.jpg"></a>
            </div><!-- .news-content -->

            <div class="news-content">
              <a href=""><img src="<?php echo base_url('assets/front'); ?>/images/thumbs/news-5.jpg"></a>
            </div><!-- .news-content -->

            <div class="news-content">
              <a href=""><img src="<?php echo base_url('assets/front'); ?>/images/thumbs/news-1.jpg"></a>
            </div><!-- .news-content -->

            <div class="news-content">
              <a href=""><img src="<?php echo base_url('assets/front'); ?>/images/thumbs/news-5.jpg"></a>
            </div><!-- .news-content -->

            <div class="news-content">
              <a href=""><img src="<?php echo base_url('assets/front'); ?>/images/thumbs/news-1.jpg"></a>
            </div><!-- .news-content -->

            <div class="news-content">
              <a href=""><img src="<?php echo base_url('assets/front'); ?>/images/thumbs/news-5.jpg"></a>
            </div><!-- .news-content -->

          </div>
          <!-- <div class="col-six p-none">
              <article class="brick entry animate-this">
                  <div class="entry-thumb">
                      <a href="#" class="thumb-link">
                         <img src="images/thumbs/news-1.jpg" width="100%" alt="">                      
                      </a>
                  </div>
              </article>
          </div> -->
        </div>
        <div class="col-three p-r-none adv-section">
            <a href="">
            <img src="<?php echo base_url('assets/front'); ?>/images/thumbs/adv1.jpg" alt="" width="100%">
          </a>
        </div>
    </div>
    </section>
    <!-- end content -->
    <!-- footer
   ================================================== -->
   <?php if(!empty($next)){ ?>   
    <a href="<?php echo base_url('Posts/news').'/'.$next[0]->postID; ?>">
      <div class="pages-link right">
        <div class="img-wrapper">
            <img src="<?php echo base_url('assets/front'); ?>/images/thumbs/news-3.jpg" alt="">
        </div>
        <div class="news-text">
           <?php echo substr(strip_tags($next[0]->postCont),0,300)."..." ?>
        </div>
        <div class="arrow">
            <i class="fa fa-angle-right" aria-hidden="true"></i>
        </div>
      </div>
    </a>
    <?php } ?>
    <?php if(!empty($previous)){ ?>
    <a href="<?php echo base_url('Posts/news').'/'.$previous[0]->postID; ?>">
      <div class="pages-link left">
          <div class="img-wrapper">
              <img src="<?php echo base_url('assets/front'); ?>/images/thumbs/news-3.jpg" alt="">
          </div>
          <div class="news-text">
              <?php echo substr(strip_tags($previous[0]->postCont),0,300)."..." ?>
          </div>
          <div class="arrow">
              <i class="fa fa-angle-left" aria-hidden="true"></i>
          </div>
      </div>
    </a>
    <?php } ?>
    <!-- footer
   ================================================== -->
    <script src="<?php echo base_url('assets/front'); ?>/js/vertical.news.slider.min.js"></script>