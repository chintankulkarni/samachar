
   <!-- footer
   ================================================== -->
   <footer>

    <div class="footer-main">

        <div class="row">  

            <div class="col-six tab-full mob-full footer-info">            

                <h4>About samachar live</h4>

                   <p>
                    Samachar Live is 360 degree global news portal with coverage within India as well as International boundaries. It initially started as post and content generating and sharing website but in short span of time, it ventured with Media Giants like PR News Wire, NASDAQ Globe News Wire, Business Wire, IANS etc. to name a few. Samachar Live believe and acts as a channel for sharing and remedying social concerns.
                    </p>
                  <p class="social-links">
                     <a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a>
                     <a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
                     <a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                     <a href=""><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
                     <a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                 </p>
              </div> <!-- end footer-info -->

            <div class="col-six tab-1-3 mob-1-2 site-links">

                <h4>Important Links</h4>

                <ul>
                    <li><a href="">national news</a></li>
                  <li><a href="">international news</a></li>
                  <li><a href="">how to</a></li>
                  <li><a href="">business news</a></li>
                  <li><a href="">start-up uupdates</a></li>
                  <li><a href="">success stories</a></li>
                  <li><a href="">bollywood news</a></li>
                  <li><a href="">movies trailer</a></li>
                  <li><a href="">telly buzz</a></li>
                  <li><a href="">hollywood news</a></li>
                  <li><a href="">movies reviews</a></li>
                  <li><a href="">beauty &ammp; fashion</a></li>
                  <li><a href="">health &amp; fitness</a></li>
                  <li><a href="">home &amp; garden</a></li>
                  <li><a href="">relationship</a></li>
                  <li><a href="">women</a></li>
                  <li><a href="">social post</a></li>
                  <li><a href="">patenting tips</a></li>
                  <li><a href="">adventure trips</a></li>
                  <li><a href="">sights</a></li>
                  <li><a href="">top detinations</a></li>
                  <li><a href="">wild life</a></li>
                  <li><a href="">pr newswire</a></li>
                  <li><a href="">lans</a></li>
                  <li><a href="">globe newswire</a></li>
                  <li><a href="">business newswire</a></li>
                    </ul>

            </div> <!-- end site-links -->  
            
          </div> <!-- end row -->

    </div> <!-- end footer-main -->

      <div class="footer-bottom">
        <div class="row">

            <div class="col-twelve">
                <div class="copyright">
                    <span>© Copyright Samachar Live 2017 All rights reserved</span>                 
                 </div>

                 <div id="go-top">
                    <a class="smoothscroll" title="Back to Top" href="#top"><i class="icon icon-arrow-up"></i></a>
                 </div>         
            </div>

        </div> 
      </div> <!-- end footer-bottom -->  

   </footer>  

   <div id="preloader"> 
        <div id="loader"></div>
   </div> 

   <!-- Java Script
   ================================================== --> 
  
   <script src="<?php echo base_url('assets/front'); ?>/js/plugins.js"></script>
   <script src="<?php echo base_url('assets/front'); ?>/js/jquery.appear.js"></script>
   <script src="<?php echo base_url('assets/front'); ?>/js/main.js"></script>

</body>

</html>